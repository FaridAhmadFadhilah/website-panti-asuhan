@auth
<div class="bg-gray-50 opacity-25 fixed inset-0 min-h-screen " x-show="popUp"></div>
<div class="fixed inset-0 flex justify-center items-center h-screen container z-20" x-show="popUp">

    <div class="lg:w-7/12">

        <div class="bg-white shadow-lg rounded-md">

            <div class="text-xl font-bold justify-between flex py-5 container">
                <a href="{{ route('user-program.show', $program->slug) }}">{{ $program->title }}</a>
                <button x-on:click="popUp = false">x</button>
            </div>

            <div class="container">
                <form action="{{ route('donation.store', $program->slug) }}" method="post" >
                    @csrf
                    <input type="hidden" name="program_id" value="{{ $program->id }}">
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" id="">
                <div class="lg:flex mb-4">
                    <label for="pay_amount" class="lg:w-6/12">Masukan Nominal Donasi</label>
                    <input type="number" name="pay_amount" class="w-full focus:ring focus:outline-none focus:ring-indigo-200 mb-6 border-gray-400" max="{{ $program->donation }}" id="donation">
                </div>

                <div class="lg:flex mb-4">
                    <label for="description" class="lg:w-6/12">Keterangan</label>
                    <textarea name="description" id="description"  rows="5" class="w-full border-gray-400 focus:outline-none focus:ring focus:ring-indigo-200" placeholder="Masukan Doa Anda"></textarea>
                </div>

                <div class="mb-4 lg:flex items-center">
                    <label for="hidden_name"><input type="checkbox" name="hidden_name" id="hidden_name" value="1"  class="mr-5 items-center">Sembunyikan Nama</label>
                </div>

                <button type="submit" class="px-4 py-2 bg-gradient-to-br from-green-400 to-blue-300 text-gray-50 rounded-lg transform hover:scale-90 mb-4 focus:ring-indigo-300">
                    Donasi
                </button>
            </form>
            </div>


        </div>

    </div>

</div>
@endauth
