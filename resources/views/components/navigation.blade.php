<div class="container" x-data="{dropdown: false, menu:false}">
        <nav class="bg-gradient-to-r from-green-400  to-blue-400  shadow-md rounded-b-md lg:w-11/12 w-10/12 container fixed z-50">

            <div class="lg:flex  items-center lg:py-5 lg:px-2 py-4 px-3">
                <div class="flex justify-between lg:border-none" :class="{'border-b-2 border-sky-50 mb-2 lg:mb-0': !menu}">
                    <a href="" class="text-xl font-bold text-white min-w-max mr-5">Panti Asuhan Baraya</a>
                    <button x-on:click="menu = !menu" class="lg:hidden text-white">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z" clip-rule="evenodd" />
                          </svg>
                    </button>
                </div>

                <div class="lg:flex lg:justify-between lg:w-full block" :class="{'hidden': menu}">
                    <div class="lg:flex  ">
                        <a href="{{ route('home') }}" class="block text-white mr-2 lg:mr-3">Beranda</a>
                        <a href="{{ route('user-program.index') }}" class="block text-white mr-2 lg:mr-3">Program Kerja</a>
                    </div>
                    <div class="lg:flex ">
                        @guest
                        <a href="{{ route('login') }}" class="block text-white mr-2 lg:mr-3">Login</a>
                        <a href="{{ route('register') }}" class="block text-white mr-2 lg:mr-3">Register</a>
                        @endguest
                        @auth
                        <div class="relative">
                            <button x-on:click="dropdown = !dropdown" class="text-white ">{{ Auth::user()->name }}</button>
                            <div x-show="dropdown" class="bg-white absolute shadow-lg rounded-md lg:w-40 w-52  text-center overflow-hidden lg:right-0 ">
                                <a href="{{ route('profile', Auth::user()->username) }}" class="hover:bg-gray-50 w-full py-1 font-sans block">Profile</a>
                                <a href="{{ route('index.invoice', Auth::user()->username) }}" class="hover:bg-gray-50 w-full py-1 font-sans block">Invoice</a>
                                <form action="{{ route('logout') }}" method="post">
                                @csrf
                                <button type="submit" class="hover:bg-gray-50 w-full py-1 font-sans">Keluar</button>
                                </form>
                            </div>
                        </div>

                        @endauth

                    </div>
                </div>
            </div>

        </nav>

</div>

