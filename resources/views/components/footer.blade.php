
<footer class=" bg-sky-400 p-4">

    <div class="container">
        <div class="flex flex-wrap">
            <div class="lg:w-4/12 lg:border-none border-b-2">
                <div class="px-4">
                    <a href="{{ route('home') }}"
                    class="text-2xl font-extrabold text-white">Panti
                    Asuhan Baraya</a>
                <p class="leading-relaxed font-serif text-justify text-emerald-50 text-base">
                    Panti Asuhan Baraya merupakan sebuah situs resmi dari panti asuhan baraya, yang dibuat untuk
                    membantu memanajemen Panti asuhan supaya panti asuhan lebih elegan.
                </p>
                </div>

            </div>
            <div class="lg:w-4/12 lg:py-0 py-5 border-separate">
                <div class="px-4">
                    <h2 class="text-2xl text-white font-bold">Kontak Kami</h2>
                <p class="text-sm text-justify font-serif text-emerald-50">Jl Raya Andir Timur Kp Ciodeng RT 01 RW 08 Desa Bojong malaka Kecamatan Baleendah  Kabupaten Bandung</p>
                <p class="leading-relaxed  my-6 bg-sky-200 border-t">

                    <div class="mb-2 flex items-center justify-between">
                        <label for="" class="text-sm text-lime-300">Email</label>

                        <p class="text-sm text-lime-100 font-medium">Lorem, ipsum.</p>
                    </div>

                    <div class="mb-2 flex items-center justify-between">
                        <label for="" class="text-sm text-lime-300">No Telepon</label>

                        <p class="text-sm text-lime-100 font-medium">0812 1453 0046</p>
                    </div>

                    <div class="mb-2 flex items-center justify-between">
                        <label for="" class="text-sm text-lime-300">Email</label>

                        <p class="text-sm text-lime-100 font-medium">Lorem, ipsum.</p>
                    </div>
                </p>
                </div>

            </div>
            <div class="w-4/12">
                <div class="px-4">
                    <h2 class="text-2xl text-white font-bold">Sosial Media</h2>
                    <div class="flex">
                        <a href="https://www.instagram.com/pantiyatimbaraya.id/">
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="48px" height="48px"><radialGradient id="yOrnnhliCrdS2gy~4tD8ma" cx="19.38" cy="42.035" r="44.899" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fd5"/><stop offset=".328" stop-color="#ff543f"/><stop offset=".348" stop-color="#fc5245"/><stop offset=".504" stop-color="#e64771"/><stop offset=".643" stop-color="#d53e91"/><stop offset=".761" stop-color="#cc39a4"/><stop offset=".841" stop-color="#c837ab"/></radialGradient><path fill="url(#yOrnnhliCrdS2gy~4tD8ma)" d="M34.017,41.99l-20,0.019c-4.4,0.004-8.003-3.592-8.008-7.992l-0.019-20	c-0.004-4.4,3.592-8.003,7.992-8.008l20-0.019c4.4-0.004,8.003,3.592,8.008,7.992l0.019,20	C42.014,38.383,38.417,41.986,34.017,41.99z"/><radialGradient id="yOrnnhliCrdS2gy~4tD8mb" cx="11.786" cy="5.54" r="29.813" gradientTransform="matrix(1 0 0 .6663 0 1.849)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#4168c9"/><stop offset=".999" stop-color="#4168c9" stop-opacity="0"/></radialGradient><path fill="url(#yOrnnhliCrdS2gy~4tD8mb)" d="M34.017,41.99l-20,0.019c-4.4,0.004-8.003-3.592-8.008-7.992l-0.019-20	c-0.004-4.4,3.592-8.003,7.992-8.008l20-0.019c4.4-0.004,8.003,3.592,8.008,7.992l0.019,20	C42.014,38.383,38.417,41.986,34.017,41.99z"/><path fill="#fff" d="M24,31c-3.859,0-7-3.14-7-7s3.141-7,7-7s7,3.14,7,7S27.859,31,24,31z M24,19c-2.757,0-5,2.243-5,5	s2.243,5,5,5s5-2.243,5-5S26.757,19,24,19z"/><circle cx="31.5" cy="16.5" r="1.5" fill="#fff"/><path fill="#fff" d="M30,37H18c-3.859,0-7-3.14-7-7V18c0-3.86,3.141-7,7-7h12c3.859,0,7,3.14,7,7v12	C37,33.86,33.859,37,30,37z M18,13c-2.757,0-5,2.243-5,5v12c0,2.757,2.243,5,5,5h12c2.757,0,5-2.243,5-5V18c0-2.757-2.243-5-5-5H18z"/></svg>
                        </a>
                        <a href="https://www.facebook.com/pantiyatim.baraya.3">
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="48px" height="48px"><linearGradient id="Ld6sqrtcxMyckEl6xeDdMa" x1="9.993" x2="40.615" y1="9.993" y2="40.615" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#2aa4f4"/><stop offset="1" stop-color="#007ad9"/></linearGradient><path fill="url(#Ld6sqrtcxMyckEl6xeDdMa)" d="M24,4C12.954,4,4,12.954,4,24s8.954,20,20,20s20-8.954,20-20S35.046,4,24,4z"/><path fill="#fff" d="M26.707,29.301h5.176l0.813-5.258h-5.989v-2.874c0-2.184,0.714-4.121,2.757-4.121h3.283V12.46 c-0.577-0.078-1.797-0.248-4.102-0.248c-4.814,0-7.636,2.542-7.636,8.334v3.498H16.06v5.258h4.948v14.452 C21.988,43.9,22.981,44,24,44c0.921,0,1.82-0.084,2.707-0.204V29.301z"/></svg>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

</footer>
