<div class=" bg-white lg:mx-4 mx-2 shadow-lg lg:my-4 my-4  rounded-lg overflow-hidden" x-data="{popUp:false}">
    @if ($program->thumbnail)
        <img src="{{ asset('storage/' . $program->thumbnail) }}" alt="" srcset=""
            class="object-cover object-center lg:h-80 h-screen w-full">
    @else
        <img src="{{ asset('frontend/img/DSHSH.jpg') }}" alt="" srcset="" class="object-cover object-center lg:h-80 h-screen w-max">
    @endif

    <a href="{{ route('user-program.show', $program->slug) }}" class="text-center flex justify-center text-lg font-bold">{{ $program->title }}</a>
    <a href="{{ route('user-program.show', $program->slug) }}" class="font-semibold text-sky-500 text-base flex justify-center my-2">Lihat Detail Program</a>
    {{-- <div class="flex p-4 justify-between">
        <div class="">
            <span class="block">Uang Terkumpul</span>
            <span>Rp.</span>
        </div>
        <div>
            <span class="block">Sisa Hari</span>
            <span>
                @if ($program->due_date >= now())
                {{ \Carbon\Carbon::parse($program->due_date)->diffForHumans() }}
                @else
                Sudah mencapai batas waktu
                @endif
            </span>
        </div>

    </div> --}}
    @if ($program->due_date >= now())
    <div class="flex flex-col items-end">
        {{-- <span class="text-xs text-gray-600 mb-1" x-text="`${Math.round(value/total * 100)}% Tercapai`"></span>
        <span class="p-3 w-full rounded-md bg-gray-200 overflow-hidden relative flex items-center">
            <span class="absolute h-full w-full bg-gradient-to-br from-green-400 to-blue-300 left-0 transition-all duration-300" :style="`width: ${ value/total * 100 }%`"></span>
        </span> --}}
    </div>

    <button x-on:click="popUp = !popUp"
    class="bg-gradient-to-tl from-red-600 py-3  to-yellow-700 text-white flex justify-center w-full">Donasi
    Sekarang</button>
    @endif


@include('components.form-donation')
</div>
