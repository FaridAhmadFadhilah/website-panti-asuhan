<div class="fixed inset-0 flex justify-start items-end h-screen" x-data="{alert:true}">
    <div class="w-full">
  <div class="lg:w-1/3 w-2/3">
        <div class="bg-gray-50 ring shadow-lg p-4 mb-4 rounded-lg" x-show="alert">
            <div class="flex justify-between border-b-4">
                <div class="font-bold">Notifikasi baru</div>
                <button class="font-bold" @click="alert = false">X</button>
            </div>
            <p class="leading-relaxed text-gray-700 text-justify">
                {{ $message }}
            </p>
        </div>
    </div>
</div>
</div>



