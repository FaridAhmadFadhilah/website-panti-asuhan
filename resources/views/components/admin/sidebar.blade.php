<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="{{ route('admin.index') }}">
          Website Panti Asuhan
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link {{ request()->routeIs('admin.index') ? 'active' : '' }}" href="{{ route('admin.index') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link {{ request()->routeIs('program.index') ? 'active' : '' }}"  href="{{ route('program.index') }}">
                <i class="ni ni-planet text-orange"></i>
                <span class="nav-link-text">Management Program Kerja</span>
              </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ request()->routeIs('pengurus.index') ? 'active' : '' }}"  href="{{ route('pengurus.index') }}">
                  <i class="ni ni-planet text-orange"></i>
                  <span class="nav-link-text">Management Pengurus Panti Asuhan</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link {{ request()->routeIs('anak.index') ? 'active' : '' }}"  href="{{ route('anak.index') }}">
                  <i class="ni ni-planet text-orange"></i>
                  <span class="nav-link-text">Management Pendataan Anak Panti Asuhan</span>
                </a>
              </li>

          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('role.index') ? 'active' : '' }}" href="{{ route('role.index') }}">
              <i class="ni ni-planet text-orange"></i>
              <span class="nav-link-text">Management Role</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('user.index') ? 'active' : '' }}" href="{{ route('user.index') }}">
              <i class="ni ni-planet text-orange"></i>
              <span class="nav-link-text">Management Users</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('keuangan.index') ? 'active' : '' }}" href="{{ route('keuangan.index') }}">
              <i class="ni ni-planet text-orange"></i>
              <span class="nav-link-text">Management Keuangan</span>
            </a>
          </li>

        </div>
      </div>
    </div>
  </nav>
