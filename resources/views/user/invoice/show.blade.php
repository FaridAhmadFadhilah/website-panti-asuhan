@extends('layouts.app')

@section('content')
    <div class="py-32 flex  justify-center">
        <div class="lg:w-8/12">

            <div class=" bg-white shadow-lg px-11 py-9 rounded-lg">
                <div class="container lg:w-10/12">
                    <h1 class="text-4xl text-center font-bold mb-7">Panti Asuhan Baraya</h1>

                    <header class="">
                        <h1 class="text-2xl font-bold">Invoice</h1>
                        <div class="mb-3 flex justify-between text-sm">
                            <label for="" class=" text-gray-600 w-6/12">No Invoice</label>
                            :
                            <p class="ml-5 text-gray-800 font-semibold w-6/12 ">
                                {{ $invoice->no_invoice }}
                            </p>
                        </div>


                        <div class="mb-3 flex justify-between text-sm">
                            <label for="" class=" text-gray-600 w-6/12">Tanggal Dibuat</label>
                            :
                            <p class="ml-5 text-gray-800 font-semibold w-6/12">
                                {{ \Carbon\Carbon::parse($invoice->created_at)->translatedFormat('d F Y, H:i') }}</p>
                        </div>

                        <div class="mb-3 flex justify-between text-sm">
                            <label for="" class=" text-gray-600 w-6/12">Tanggal Kadaluarsa</label>
                            :
                            <p class="ml-5 text-gray-800 font-semibold w-6/12">
                                {{ \Carbon\Carbon::parse($invoice->program->due_date)->translatedFormat('d F Y, H:i') }}
                            </p>
                        </div>
                    </header>

                    <main class="">
                        <div class="flex border-t-2 py-6">
                            <div class="w-full">

                                <div class="flex justify-between">
                                    <div class="">
                                        <h2 class="font-semibold text-gray-900 border-b-2 border-black text-lg mb-3">
                                            Imformasi
                                            Panti Asuhan</h2>
                                        <p class="font-semibold text-gray-600 text-sm mb-2">Panti Asuhan Baraya</p>
                                        <p class="font-semibold text-gray-600 text-sm mb-2">Telp : 085323910018</p>
                                        <p class="font-semibold text-gray-600 text-sm mb-2">Email : farid@admin.com</p>
                                    </div>
                                    <div class="">
                                        <h2 class="font-semibold text-gray-900 border-b-2 border-black text-lg mb-3">
                                            Imformasi
                                            pendonasi</h2>
                                        <p class="font-semibold text-gray-600 text-sm mb-2">{{ $invoice->user->name }}</p>
                                        <p class="font-semibold text-gray-600 text-sm mb-2">Telp : -</p>
                                        <p class="font-semibold text-gray-600 text-sm mb-2">Email :
                                            {{ $invoice->user->email }}</p>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <div class="flex border-t-2 py-6">
                            <div class="overflow-x-auto">
                                <table class="table-auto mt-8">

                                    <thead
                                        class="uppercase bg-gradient-to-br from-emerald-300 to-sky-500 text-sm text-emerald-50">
                                        <tr>
                                            <th class="p-2 whitespace-nowrap">
                                                <div class="font-semibold text-left">
                                                    Nama Program Yang didonasikan
                                                </div>
                                            </th>
                                            <th class="p-2 whitespace-nowrap">
                                                <div class="font-semibold text-left">
                                                    Nominal Donasi
                                                </div>
                                            </th>
                                            <th class="p-2 whitespace-nowrap">
                                                <div class="font-semibold text-left">
                                                    Status
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody class="text-sm">
                                        <tr>
                                            <td class="p-2 whitespace-nowrap">
                                                <div class="text-left">
                                                    {{ $invoice->program->title }}
                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                <div class="text-left font-medium text-green-500">
                                                    {{ $invoice->pay_amount }}
                                                </div>
                                            </td>
                                            <td class="p-2 whitespace-nowrap">
                                                <div class="inline-flex bg-red-500 text-red-50 p-1 rounded-md">
                                                    {{ $invoice->status }}
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <div class="flex border-t-2 py-6">
                            <div>
                                <h2 class="font-semibold text-gray-800 border-b-2 border-black text-xl mb-3 inline-flex">
                                    Pesan</h2>
                                <p class="leading-relaxed text-justify text-gray-700">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi, rerum dignissimos!
                                    Reprehenderit laborum ducimus commodi quidem magnam, assumenda modi quisquam voluptatum
                                    sed nihil distinctio quia laboriosam eum repellendus quos qui!
                                </p>
                            </div>
                        </div>

                        <div class="flex justify-center">
                            @if ($invoice->status === "Belum Dibayar")
                                <button id="pay-button"
                                    class="bg-gradient-to-br from-sky-600 to-sky-400 py-2 px-4 rounded-lg uppercase text-sky-50 font-semibold">Bayar
                                    Sekarang</button>
                            @endif

                            {{-- <pre><div id="result-json">JSON result will appear here after payment:<br></div></pre> --}}
                        </div>

                    </main>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js"
        integrity="sha512-u9akINsQsAkG9xjc1cnGF4zw5TFDwkxuc9vUp5dltDWYCSmyd0meygbvgXrlc/z7/o4a19Fb5V0OUE58J7dcyw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ env('MIDTRANS_CLIENT_KEY') }}">
    </script>
    <script type="text/javascript">
        document.getElementById('pay-button').onclick = async function() {
            const response = await axios.get("{{ route('donation.pay', $invoice->no_invoice) }}")
            if (response.status === 200) {
                // SnapToken acquired from previous step
                snap.pay(response.data, {
                    // Optional
                    onSuccess: function(result) {
                        /* You may add your own js here, this is just example */
                        document.getElementById('result-json').innerHTML += JSON.stringify(result, null,
                            2);
                    },
                    // Optional
                    onPending: function(result) {
                        /* You may add your own js here, this is just example */
                        document.getElementById('result-json').innerHTML += JSON.stringify(result, null,
                            2);
                    },
                    // Optional
                    onError: function(result) {
                        /* You may add your own js here, this is just example */
                        document.getElementById('result-json').innerHTML += JSON.stringify(result, null,
                            2);
                    }
                });

            }
        };
    </script>

@endsection
