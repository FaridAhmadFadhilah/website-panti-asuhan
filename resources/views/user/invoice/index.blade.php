@extends('layouts.app')

@section('header')
    <div class="w-screen max-h-screen overflow-hidden bg-gradient-to-br from-sky-400 to-emerald-300 py-32">

        <div class="flex flex-wrap container">
            <div class="lg:w-6/12">

                <div class="leading-relaxed text-justify">
                    <h2 class="lg:text-2xl text-lg">
                       <div class="text-lime-50 font-semibold">
                        Lorem ipsum dolor sit amet
                        </div>
                    </h2>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere nam debitis, pariatur expedita, maiores ex voluptas, qui placeat distinctio incidunt similique sapiente consequatur sunt velit a dolor omnis. Consequatur, adipisci.
                </div>

            </div>
        </div>

    </div>
@endsection

@section('content')

    <div class="flex justify-center">
        <div class="lg:w-max lg:max-w-none w-11/12">
            <div class="bg-white shadow-lg">
                <header class="py-4 border-gray-100 flex justify-around">
                    <h2 class="font-semibold text-gray-800 text-left"></h2>
                </header>
                <div class="flex justify-around ">
                    <div class="overflow-x-auto">
                        <table class="table-auto rounded-lg">
                            <thead
                                class="uppercase bg-gradient-to-br from-emerald-300 to-sky-500 lg:text-sm text-xs text-emerald-50">
                                <tr>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">
                                            No
                                        </div>
                                    </th>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">
                                            No Invoice
                                        </div>
                                    </th>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">
                                            Nama Program
                                        </div>
                                    </th>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">
                                            Nominal
                                        </div>
                                    </th>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">
                                            Status
                                        </div>
                                    </th>
                                    <th class="p-2 whitespace-nowrap">
                                        <div class="font-semibold text-left">
                                            Opsi
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="lg:text-sm text-xs leading-relaxed">
                                @foreach ($invoices as $index => $invoice)
                                    <tr class=" border-b border-sky-100 bg-sky-50">
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left">
                                                {{ $index + 1 }}
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left text-sky-500 italic font-serif ">
                                                <a
                                                    href="{{ route('show.invoice', [Auth::user()->username, $invoice->no_invoice]) }}">{{ $invoice->no_invoice }}</a>
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left">
                                                {{ $invoice->program->title }}
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="text-left font-medium text-green-500">
                                                {{ $invoice->pay_amount }}
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <div class="inline-flex bg-red-500 text-red-50 p-1 rounded-md">
                                                {{ $invoice->status }}
                                            </div>
                                        </td>
                                        <td class="p-2 whitespace-nowrap">
                                            <a href="{{ route('show.invoice', [$invoice->user->username, $invoice->no_invoice]) }}"
                                                class="capitalize py-1 bg-lime-400 text-emerald-50 rounded-lg px-2 font-serif">
                                                Lihat detail
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>

    </div>


@endsection
