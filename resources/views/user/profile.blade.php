@extends('layouts.app')

@section('header')

    <div class="w-screen max-h-screen overflow-hidden bg-gradient-to-br from-sky-400 to-emerald-300 py-32">


        <div class="flex flex-wrap container">

            <div class="lg:w-7/12 ">
                <div class="flex items-center">
                    <img src="https://scontent-cgk1-2.xx.fbcdn.net/v/t39.30808-1/p200x200/215673073_112872404395861_4551524961946979699_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=7206a8&_nc_eui2=AeG8Tah1coKuxKY3v2ZxbBKwANcO86CAhfEA1w7zoICF8fICujr08mJDFY_cbaWl1iQqQ7ukJE5oJeMJA9UKoNWf&_nc_ohc=bQkC-nhxK30AX8qMgwU&_nc_ht=scontent-cgk1-2.xx&oh=f50ef7d6aa2d33c63052e641dbb5aedd&oe=61B214ED"
                        alt="" srcset="" class="rounded-full object-cover object-center lg:h-20 h-20 mr-5 ring ring-white mb-6">
                    <div>
                        <a href="" class="font-bold text-2xl block text-white">{{ $user->name }}</a>
                        <span class="text-sm font-light text-emerald-50">Bergabung
                            {{ $user->created_at->format('d F Y') }}</span>
                    </div>
                </div>
                <p class="leading-relaxed font-medium text-lime-50">Kami tidak tahu banyak tentang dia, tapi kami yakin
                    {{ $user->name }} adalah seseorang yang hebat.</p>
            </div>

            <hr class="lg:hidden">


            <div class="lg:w-5/12 w-11/12">

                <div class="flex flex-wrap">
                    <div class="w-6/12">

                        <div class="bg-lime-200  rounded-lg px-5 lg:h-80 h-80 flex justify-center items-center">

                            <div class="flex justify-center items-center flex-col">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20 text-center mb-7 text-lime-700" viewBox="0 0 20 20"
                                    fill="currentColor">
                                    <path
                                        d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
                                </svg>

                                <p class="flex flex-col text-center">
                                    <span class=" text-3xl bg-gradient-to-tr from-sky-400 to-emerald-500 text-transparent inline bg-clip-text font-bold">Rp. <i></i></span>
                                    <span class="text-xl">Total donasi</span>
                                </p>
                            </div>



                        </div>

                    </div>
                </div>

            </div>


        </div>

    </div>

@endsection
