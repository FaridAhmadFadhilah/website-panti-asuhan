@extends('layouts.app')

@section('content')
<div class="flex justify-center items-center h-screen">
   <div class="lg:w-5/12 w-8/12">

    <form action="{{ route('login') }}" method="post">
        @csrf
    <div class="bg-white shadow-lg rounded-md p-10">
        <div class="text-center text-2xl py-5 font-bold">Login</div>
        <div class="mb-4">
            <label for="email" class="block">Email</label>
            <input type="email" name="email" id="email" class="form-input w-full border-gray-300" value="{{ old('email') }}">
        </div>
        <div class="mb-4">
            <label for="password" class="block">Password</label>
            <input type="password" name="password" id="password" class="form-input w-full border-gray-300">
        </div>
        <div class="mb-4 flex justify-between">
            <a href="{{ route('register') }}" class="inline-flex text-blue-600">Buat Akun Baru ?</a>
            <a href="" class="inline-flex text-blue-600">Lupa Password ?</a>
        </div>

        <button type="submit" class="px-4 py-2 bg-blue-400 hover:bg-blue-500 transform hover:scale-90 text-white">Login</button>
    </div>
</form>
   </div>
</div>
@endsection
