@extends('layouts.app')

@section('content')
<div class="flex justify-center">
   <div class="lg:w-5/12 w-8/12">

    <form action="{{ route('register') }}" method="post">
        @csrf
    <div class="bg-white shadow-lg rounded-md mt-20 p-10">
        <div class="text-center text-2xl py-5 font-bold">Daftar</div>
        <div class="mb-4">
            <label for="nama" class="block">Masukan Nama Pengguna</label>
            <input type="text" name="name" id="nama" class="form-input w-full border-gray-300" value="{{ old('name') }}">
            <span class="text-red-500 italic">
                @error('name')
                {{ $message }}
                @enderror
            </span>
        </div>
        <div class="mb-4">
            <label for="email" class="block">Email</label>
            <input type="email" name="email" id="email" class="form-input w-full border-gray-300" value="{{ old('email') }}">
            <span class="text-red-500 italic">
                @error('email')
                {{ $message }}
                @enderror
            </span>
        </div>

        <div class="mb-4">
            <label for="username" class="block">Username</label>
            <input type="text" name="username" id="username" class="form-input w-full border-gray-300" value="{{ old('username') }}">
            <span class="text-red-500 italic">
                @error('username')
                {{ $message }}
                @enderror
            </span>
        </div>

        <div class="mb-4">
            <label for="password" class="block">Password</label>
            <input type="password" name="password" id="password" class="form-input w-full border-gray-300">
            <span class="text-red-500 italic">
                @error('password')
                {{ $message }}
                @enderror
            </span>
        </div>
        <div class="mb-4">
            <label for="password" class="block">Konfirmasi Password</label>
            <input type="password" name="password_confirmation" id="password" class="form-input w-full border-gray-300">
            <span class="text-red-500 italic">
                @error('password')
                {{ $message }}
                @enderror
            </span>
        </div>
        <div class="mb-4">
            <a href="{{ route('login') }}" class="inline-flex text-blue-600">Sudah punya akun ?</a>
        </div>

        <button type="submit" class="px-4 py-2 bg-blue-400 hover:bg-blue-500 transform hover:scale-90 text-white">Daftar</button>
    </div>
</form>
   </div>
</div>
@endsection
