@extends('layouts.app')

@section('header')
    <x-carousel />
    <div class="container" x-data="{popUp:false}">

        <div>
            <a href="{{ route('user-program.index') }}"
                class=" text-3xl font-bold text-gray-800">Program Kebaikan</a>

            <div class="flex flex-wrap">

                @foreach ($programs as $program)

                    <div class="lg:w-4/12 md:w-6/12 w-11/12" x-data="{value:0, total:{{ $program->donation }}}">

                       @include('components.card-program')

                    </div>

                @endforeach

            </div>
        </div>


    </div>


@endsection
