@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">

            <div class="card" style="width: 70vw;">
                <form action="{{ route('pengurus.update', $penguru->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <div class="card-title text-center">
                            <h4>Edit Pengurus : {{ $penguru->nama }}</h4>
                        </div>

                        <input type="hidden" name="oldImage" value="{{ $penguru->profile }}">
                        <div class="mb-4">
                            <label for="nama" class="form-label">Nama Lengkap</label>
                            <input type="text" name="nama" id="nama" value="{{ $penguru->nama }}" class="form-control">
                            @error('nama')
                            <span class="text-danger italic">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="mb-4">
                            <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control"
                                value="{{ $penguru->tempat_lahir }}">
                                @error('tempat_lahir')
                                <span class="text-danger italic">
                                    {{ $message }}
                                </span>
                                @enderror
                        </div>

                        <div class="mb-4">
                            <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                            <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control"
                                value="{{ $penguru->tanggal_lahir }}">
                                @error('tanggal_lahir')
                                <span class="text-danger italic">
                                    {{ $message }}
                                </span>
                                @enderror
                        </div>

                        <div class="mb-4">
                            <div class="form-check">
                                <input type="radio" name="jk" id="l" value="Laki-laki" class="form-check-input"
                                    {{ $penguru->jk == 'Laki-laki' ? 'checked' : '' }}>
                                <label for="l" class="form-check-label">Laki-laki</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" name="jk" id="p" value="Perempuan" class="form-check-input"
                                    {{ $penguru->jk == 'Perempuan' ? 'checked' : '' }}>
                                <label for="p" class="form-check-label">Perempuan</label>
                            </div>
                            @error('jk')
                            <span class="text-danger italic">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="mb-4">
                            <label for="alamat" class="form-label">Alamat</label>
                            <textarea name="alamat" id="" cols="30" rows="10"
                                class="form-control">{{ $penguru->alamat }}</textarea>
                                @error('alamat')
                                <span class="text-danger italic">
                                    {{ $message }}
                                </span>
                                @enderror
                        </div>

                        @if ($penguru->jabatan != 'Ketua')
                        <div class="mb-4">
                            <label for="jabatan" class="form-label">Jabatan</label>
                            <select name="jabatan" id="jabatan" class="form-control">
                                <option value="Dewan Pembina"
                                {{ $penguru->jabatan === 'Dewan Pembina' ? 'selected' : '' }}>Dewan Pembina</option>
                                <option value="Ketua" {{ $penguru->jabatan === 'Ketua' ? 'selected' : '' }}>Ketua
                                </option>
                                <option value="Sekertaris" {{ $penguru->jabatan === 'Sekertaris' ? 'selected' : '' }}>
                                    Sekertaris</option>
                                <option value="Bendahara" {{ $penguru->jabatan === 'Bendahara' ? 'selected' : '' }}>
                                    Bendahara</option>
                                <option value="Bidang Dewan PembinaLogistik"
                                    {{ $penguru->jabatan === 'Bidang Logistik' ? 'selected' : '' }}>Bidang Logistik
                                </option>
                                <option value="Bidang Pendidikan"
                                    {{ $penguru->jabatan === 'Bidang Pendidikan' ? 'selected' : '' }}>Bidang Pendidikan
                                </option>
                                <option value="Bidang Kesehatan"
                                    {{ $penguru->jabatan === 'Bidang Kesehatan' ? 'selected' : '' }}>Bidang Kesehatan
                                </option>
                                <option value="Bidang Perawatan"
                                    {{ $penguru->jabatan === 'Bidang Perawatan' ? 'selected' : '' }}>Bidang Perawatan
                                </option>
                                <option value="Bagian Asrama"
                                    {{ $penguru->jabatan === 'Bagian Asrama' ? 'selected' : '' }}>Bagian Asrama</option>
                                <option value="Bagian Dapur"
                                    {{ $penguru->jabatan === 'Bagian Dapur' ? 'selected' : '' }}>Bagian Dapur</option>
                                <option value="Bagian Pengasuh"
                                    {{ $penguru->jabatan === 'Bagian Pengasuh' ? 'selected' : '' }}>Bagian Pengasuh
                                </option>
                            </select>
                            @error('jabatan')
                            <span class="text-danger italic">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>
                        @endif


                        <div class="mb-4">
                            <label for="agama" class="form-label">Agama</label>
                            <select name="agama" id="agama" class="form-control">
                                <option value="Islam" {{ $penguru->agama == 'Islam' ? 'selected' : '' }}>Islam</option>
                                <option value="Kristen" {{ $penguru->agama == 'Kristen' ? 'selected' : '' }}>Kristen
                                </option>
                                <option value="Hindu" {{ $penguru->agama == 'Hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="Budha" {{ $penguru->agama == 'Budha' ? 'selected' : '' }}>Budha</option>
                                <option value="Konghuchu" {{ $penguru->agama == 'Konghuchu' ? 'selected' : '' }}>
                                    Konghuchu</option>
                            </select>
                            @error('agama')
                            <span class="text-danger italic">
                                {{ $message }}
                            </span>
                            @enderror
                        </div>

                        <div class="mb-4">
                            <label for="telepon" class="form-label">Telepon</label>
                            <input type="text" name="telepon" id="telepon" class="form-control"
                                value="{{ $penguru->telepon }}">
                                @error('telepon')
                                <span class="text-danger italic">
                                    {{ $message }}
                                </span>
                                @enderror
                        </div>

                        <div class="mb-4">
                            <label for="Email" class="form-label">Email</label>
                            <input type="email" name="email" id="email" class="form-control"
                                value="{{ $penguru->email }}">
                                @error('email')
                                <span class="text-danger italic">
                                    {{ $message }}
                                </span>
                                @enderror
                        </div>

                        <div class="mb-4">
                            <label for="status" class="form-label">Status</label>
                            <input type="text" name="status" id="status" value="{{ $penguru->status }}"
                                class="form-control">
                                @error('status')
                                <span class="text-danger italic">
                                    {{ $message }}
                                </span>
                                @enderror
                        </div>

                        @if ($penguru->profile)
                            <img src="{{ asset('storage/' . $penguru->profile) }}" class="image-preview col-md-4 my-4">
                        @else
                            <img class="image-preview col-md-4">
                        @endif
                        <input type="file" name="profile" id="profile" class="form-control" onchange="previewImage()">

                        <button type="submit" class="btn btn-primary">Edit data pengurus</button>
                    </div>
                </form>
            </div>

        </div>

    <script>
        function previewImage() {
            const image = document.querySelector('#profile')
            const imgPreview = document.querySelector('.image-preview')

            imgPreview.style.display = 'block'

            const oFReader = new FileReader()
            oFReader.readAsDataURL(image.files[0])

            oFReader.onload = function(event) {
                imgPreview.src = event.target.result
            }
        }
    </script>
@endsection
