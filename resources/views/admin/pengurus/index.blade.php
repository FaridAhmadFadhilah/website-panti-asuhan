@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">

            <div class="card" style="width: 70vw;">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h3>Data Pengurus</h3>
                    </div>
                    @can('pengurus-create')
                        <a href="{{ route('pengurus.create') }}" class="btn btn-primary my-2">Tambah data pengurus</a>
                    @endcan
                    <div class="table-responsive">
                        <table class="table table-striped">

                            <thead>
                                <tr>
                                    <th class="col">No</th>
                                    <th class="col">Nama Pengurus</th>
                                    <th class="col">Jabatan</th>
                                    <th class="col">Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($penguruses as $index => $pengurus)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $pengurus->nama }}</td>
                                        <td>{{ $pengurus->jabatan }}</td>
                                        <td class="d-flex">
                                            @can('pengurus-edit')
                                                <a href="{{ route('pengurus.edit', $pengurus->id) }}"
                                                    class="btn btn-primary">Edit</a>
                                            @endcan
                                            @can('pengurus-show')
                                            <a href="{{ route('pengurus.show', $pengurus->id) }}"
                                                class="btn btn-info text-white">Show</a>
                                            @endcan

                                            @can('pengurus-delete')
                                                <form action="{{ route('pengurus.destroy', $pengurus->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-danger"
                                                        onclick="return confirm('Apakah anda yakin akan menghapus data ini?')">Delete</button>
                                                </form>
                                            @endcan

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>

    </div>
@endsection
