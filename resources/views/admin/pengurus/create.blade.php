@extends('layouts.admin')

@section('main-content')
<div class="d-flex justify-content-center" style="padding-top: 50px">

        <div class="card" style="width: 70vw;">
            <form action="{{ route('pengurus.store') }}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body">
                <div class="card-title text-center">Tambah Data Pengurus</div>

                <div class="mb-4">
                    <label for="nama" class="form-label">Nama Pengurus</label>
                    <input type="text" name="nama" id="nama" class="form-control" value="{{ old('nama') }}">
                @error('nama')
                  <span class="text-danger">
                        <i>
                            {{ $message }}
                        </i>
                    </span>
                @enderror
                </div>

                <div class="mb-4">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
                    @error('email')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="{{ old('tempat_lahir') }}">
                    @error('tempat_lahir')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="tanggal_ahir" class="form-label">Tanggal Lahir</label>
                    <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="{{ old('tanggal_lahir') }}">
                    @error('tanggal_lahir')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label class="form-label">Jenis Kelamin</label>
                    <div class="form-check">
                    <input type="radio" name="jk" id="l" value="Laki-laki" class="form-check-input">
                    <label for="l" class="form-check-label">Laki-laki</label>
                    </div>
                    <div class="form-check">
                        <input type="radio" name="jk" id="p" value="Perempuan" class="form-check-input">
                        <label for="p" class="form-check-label">Perempuan</label>
                    </div>
                    @error('jk')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="alamat" class="form-label">Alamat</label>
                    <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control">{{ old('alamat') }}</textarea>
                    @error('alamat')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="jabatan" class="form-label">Jabatan</label>
                    <select name="jabatan" id="jabatan" class="form-control" required>
                        <option value="">Pilih Jabatan</option>
                        <option value="Dewan Pembina">Dewan Pembina</option>
                        <option value="Ketua">Ketua</option>
                        <option value="Sekertaris">Sekertaris</option>
                        <option value="Bendahara">Bendahara</option>
                        <option value="Bidang Logistik">Bidang Logistik</option>
                        <option value="Bidang Pendidikan">Bidang Pendidikan</option>
                        <option value="Bidang Kesehatan">Bidang Kesehatan</option>
                        <option value="Bidang Perawatan">Bidang Perawatan</option>
                        <option value="Bagian Asrama">Bagian Asrama</option>
                        <option value="Bagian Dapur">Bagian Dapur</option>
                        <option value="Bagian Pengasuh">Bagian Pengasuh</option>
                    </select>
                    @error('jabatan')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="telepon" class="form-label">No Telepon</label>
                    <input type="text" name="telepon" id="telepon" class="form-control" value="{{ old('telepon') }}">
                    @error('telepon')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="status" class="form-label">Status</label>
                    <input type="text" name="status" id="status" class="form-control" value="{{ old('status') }}">
                    @error('status')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <div class="mb-4">
                    <label for="agama" class="form-label">Agama</label>
                    <select name="agama" id="agama" class="form-control">
                        <option value="">Pilih Agama</option>
                        <option value="Islam">Islam</option>
                        <option value="Kristen">Kristen</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Budha">Budha</option>
                        <option value="Konghuchu">Konghuchu</option>
                    </select>
                    @error('agama')
                    <span class="text-danger">
                          <i>
                              {{ $message }}
                          </i>
                      </span>
                  @enderror
                </div>

                <img class="image-preview col-md-4">

                <div class="mb-4">
                    <label for="profile" class="form-label">Profile</label>
                    <input type="file" name="profile" id="profile" class="form-control" onchange="previewImage()">
                </div>

                <button type="submit" class="btn btn-primary">Tambah Data</button>
            </div>
            </form>

        </div>

</div>

<script>
    function previewImage(){
        const image = document.querySelector('#profile')
        const imgPreview = document.querySelector('.image-preview')

        imgPreview.style.display = 'block'

        const oFReader = new FileReader()
        oFReader.readAsDataURL(image.files[0])

        oFReader.onload = function(event){
            imgPreview.src = event.target.result
        }
    }
</script>
@endsection
