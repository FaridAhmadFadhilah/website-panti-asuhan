@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 62vw;">
            <div class="card-body">
                <div class="card-title text-center">
                    <h4><b>Imformasi Pengurus</b></h4>
                </div>
                <div class="d-flex m-4">
                    @if ($penguru->profile)
                        <img src="{{ asset('storage/' . $penguru->profile) }}" width="270" alt="{{ $penguru->name }}">
                    @else
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQAvct3AiGO8M7vUjNZuZi9arwoBar37GL1kA&usqp=CAU"
                            alt="" srcset="">
                    @endif
                    <div class="container">
                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Nama Lengkap
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->nama }}
                            </div>
                        </div>

                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Jenis Kelamin
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->jk }}
                            </div>
                        </div>
                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Agama
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->agama }}
                            </div>
                        </div>
                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Tempat Lahir
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->tempat_lahir }}
                            </div>
                        </div>
                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Tanggal Lahir
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->tanggal_lahir }}
                            </div>
                        </div>
                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                No. Telepon
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->telepon }}
                            </div>
                        </div>
                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Alamat Email
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->email }}
                            </div>
                        </div>

                        <div class="d-flex" style="margin-bottom: 5px">
                            <div class="col-md-4">
                                Jabatan
                            </div>
                            <div class="col-md-8"> :
                                {{ $penguru->jabatan }}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
@endsection
