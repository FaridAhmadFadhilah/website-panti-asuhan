@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">

            <div class="card" style="width: 70vw;">
                <form action="{{ route('program.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="card-title text-center">Tambah Program</div>
                        <div class="mb-4">
                            <img class="image-preview col-md-4 my-4">
                            <input type="file" name="thumbnail" id="thumbnail" class="form-control form-control-lg"
                                onchange="previewImage()">
                            @error('thumbnail')
                                <span class="text-red font-italic">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="title" class="form-label">Masukan Judul Program</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                            @error('title')
                                <span class="text-red font-italic">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="label-form">Masukan Konten Program</label>
                            <textarea name="body" id="body" class="ckeditor form-control">{{ old('body') }}</textarea>
                            @error('body')
                                <span class="text-red font-italic">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="donation" id="donation" class="form-label">Masukan Nominal Maksimal
                                Donasi</label>
                            <input type="number" name="donation" id="donation" class="form-control"
                                value="{{ old('donation') }}">
                            @error('donation')
                                <span class="text-red font-italic">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <div class="mb-4">
                            <label for="due_date" id="due_date" class="form-label">Masukan Jangka Waktu Program</label>
                            <input type="datetime-local" name="due_date" id="due_date" class="form-control"
                                value="{{ old('due_date') }}">
                            @error('due_date')
                                <span class="text-red font-italic">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">
                            Masukan
                        </button>
                    </div>
                </form>
            </div>

    </div>

    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });

        CKEDITOR.replace('body', {
            filebrowserUploadUrl: "{{ route('ckeditor.image-upload', ['_token' => csrf_token()]) }}",
            filebrowserUploadMethod: 'form'
        });

        function previewImage() {
            const image = document.querySelector('#thumbnail')
            const imgPreview = document.querySelector('.image-preview')

            imgPreview.style.display = 'block'

            const oFReader = new FileReader()
            oFReader.readAsDataURL(image.files[0])

            oFReader.onload = function(event) {
                imgPreview.src = event.target.result
            }
        }
    </script>
@endsection
