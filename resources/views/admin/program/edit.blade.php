@extends('layouts.admin')

@section('main-content')
<div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">
            <div class="card-body">
                <div class="card-title text-center"><h4>Edit Program <i>{{ $program->title }}</i></h4></div>

                <form action="{{ route('program.update', $program->slug) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <input type="hidden" name="oldImage" value="{{ $program->thumbnail }}">
                <div class="mb-4">
                    @if ($program->thumbnail)
                    <img src="{{ asset('storage/'.$program->thumbnail) }}" class="image-preview col-md-4 my-4">
                    @else
                    <img class="image-preview col-md-4 my-4">
                    @endif
                    <input type="file" name="thumbnail" id="thumbnail" value="{{ $program->thumbnail }}" class="form-control form-control-lg" onchange="previewImage()">
                    @error('thumbnail')
                    <span class="text-red font-italic">
                        {{ $message }}
                    </span>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="title" class="form-label">Judul Program</label>
                    <input type="text" name="title" id="title" value="{{ $program->title }}" class="form-control">
                </div>

                <div class="mb-4">
                    <label for="donation" class="form-label">Target Donasi</label>
                    <input type="number" name="donation" id="donation" value="{{ $program->donation }}" class="form-control">
                </div>

                <div class="mb-4">
                    <label for="body" class="form-label">body</label>
                    <textarea name="body" id="body" cols="30" rows="10" class="ckeditor form-control">{!! $program->body !!}</textarea>
                </div>

                <div class="mb-4">
                    <label for="due_date" class="form-label">Jangka Waktu</label>
                    <input type="datetime" name="due_date" id="due_Date" value="{{ $program->due_date }}" class="form-control">
                </div>

                <button type="submit" class="btn btn-primary">Edit</button>
            </form>

            </div>
        </div>

</div>

<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script>
    $(document).ready(function () {
    $('.ckeditor').ckeditor();
    });

    CKEDITOR.replace('body', {
    filebrowserUploadUrl: "{{route('ckeditor.image-upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'
});

    function previewImage(){
        const image = document.querySelector('#thumbnail')
        const imgPreview = document.querySelector('.image-preview')

        imgPreview.style.display = 'block'

        const oFReader = new FileReader()
        oFReader.readAsDataURL(image.files[0])

        oFReader.onload = function(event){
            imgPreview.src = event.target.result
        }
    }
</script>
@endsection
