@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">

            <div class="card-header">
                <h3>
                    Program Kerja Panti Asuhan
                </h3>
            </div>
            
            <div class="card-body">
                @can('program-create')
                    <a href="{{ route('program.create') }}" class="btn btn-primary"
                        style="margin-top: 10px; margin-bottom: 10px;">Tambah</a>
                @endcan

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Program</th>
                                <th scope="col">Target Donasi</th>
                                <th scope="col">Jangka Waktu</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($programs as $index => $program)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $program->title }}</td>
                                    <td>{{ $program->donation }}</td>
                                    <td>{{ \Carbon\Carbon::parse($program->due_date)->translatedFormat('d F Y, H:i') }}
                                    </td>
                                    <td class="d-flex">
                                        <a href="{{ route('user-program.show', $program->slug) }}"
                                            class="btn btn-primary">Show</a>
                                        @can('program-edit')
                                            <a href="{{ route('program.edit', $program->slug) }}"
                                                class="btn btn-info text-white">Edit</a>
                                        @endcan
                                        @can('program-delete')
                                            <form action="{{ route('program.destroy', $program->slug) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger"
                                                    onclick="return confirm('Apakah anda yakin akan menghapus?')">Delete</button>
                                            </form>
                                        @endcan

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>


@endsection
