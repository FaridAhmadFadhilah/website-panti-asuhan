@extends('layouts.admin')

@section('main-content')

<div class="d-flex justify-content-center" style="padding-top: 50px;">
    <div class="card" style="width: 70vw;">
        <div class="card-body">
            <div class="card-title text-center">
                Pengeluaran
            </div>
            <a href="{{ route('pengeluaran.create') }}" class="btn btn-primary mb-3">Tambah Pengeluaran</a>

            <div class="table-responsive">
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Yang Mengeluarkan</th>
                            <th>Nominal Pengeluaran</th>
                            <th>Detail Pengeluaran</th>
                            <th>Tanggal Pengeluaran</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($spendings as $index => $spending)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $spending->user->name }}</td>
                            <td>{{ $spending->amount }}</td>
                            <td>{{ $spending->description }}</td>
                            <td>{{ \Carbon\Carbon::parse($spending->release_date)->translatedFormat('d F Y, H:i') }}</td>
                            <td class="d-flex">
                                <a href="{{ route('pengeluaran.edit', $spending->id) }}" class="btn btn-primary">Edit</a>
                                <form action="{{ route('pengeluaran.destroy', $spending->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection
