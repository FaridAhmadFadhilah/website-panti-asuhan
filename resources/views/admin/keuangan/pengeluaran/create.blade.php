@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">
        <div class="card" style="width: 70vw;">
            <div class="card-body">
                <div class="card-title text-center">Tambah Pengeluaran</div>
                <form action="{{ route('pengeluaran.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="amount" class="form-label">Nominal Pengeluaran</label>
                    <input type="number" name="amount" id="amount" class="form-control" value="{{ old('amount') }}">
                    @error('amount')
                    <i class="text-danger">{{ $message }}</i>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="description" class="form-label">Detail Pengeluaran</label>
                    <textarea name="description" id="description"  class="form-control">{{ old('description') }}</textarea>
                    @error('description')
                    <i class="text-danger">{{ $message }}</i>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah Pengeluaran</button>
            </form>


            </div>
        </div>
    </div>
@endsection
