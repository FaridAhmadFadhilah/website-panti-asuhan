@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">
        <div class="card" style="width: 70vw;">

            <div class="card-body">
                <div class="card-title text-center">
                    Edit Pengeluaran
                </div>

                <form action="{{ route('pengeluaran.update', $pengeluaran->id) }}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="amount" class="form-label">Nominal Pengeluaran</label>
                    <input type="number" name="amount" id="amount" class="form-control" value="{{ $pengeluaran->amount }}">
                </div>

                <div class="form-group">
                    <label for="description" class="form-label">Detail Pengeluaran</label>
                    <textarea name="description" id="description"  class="form-control">{{ $pengeluaran->description }}</textarea>
                </div>

                <button type="submit" class="btn btn-primary">Edit Pengeluaran</button>
            </form>
            </div>

        </div>
    </div>
@endsection
