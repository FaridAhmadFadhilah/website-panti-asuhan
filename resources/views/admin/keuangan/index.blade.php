@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px">

        <div class="col-md-12" >
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-stats" style="padding: 12px;">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Pemasukan</h5>
                                    <span class="h2 font-weight-bold mb-0">Rp. {{ number_format($donations->sum('invoice.pay_amount'), 2, ',', ',') }}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                        <i class="ni ni-chart-bar-32"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="my-3">
                                <a href="{{ route('pemasukan.index') }}" class="btn btn-info text-light">Lihat Detail</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-stats" style="padding: 12px;">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Pengeluaran</h5>
                                    <span class="h2 font-weight-bold mb-0">Rp. {{ number_format($spendings->sum('amount'), 2, ',', ',') }}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                        <i class="ni ni-chart-pie-35"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="my-3">
                                <a href="{{ route('pengeluaran.index') }}" class="btn btn-info text-light">Lihat Detail</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-stats" style="padding: 12px;">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Saldo</h5>
                                    <span class="h2 font-weight-bold mb-0">Rp. @php
                                        $total = $donations->sum('invoice.pay_amount')+$spendings->sum('amount')
                                    @endphp
                                    {{ number_format($total, 2, ',', ',') }}
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                        <i class="ni ni-money-coins"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    @endsection
