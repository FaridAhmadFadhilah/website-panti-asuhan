@extends('layouts.admin')

@section('main-content')

<div class="d-flex justify-content-center" style="padding-top: 50px;">

    <div class="card" style="width: 70vw">
        <div class="card-title text-center my-3">Pemasukan</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">No Invoice</th>
                            <th scope="col">Nama Program</th>
                            <th scope="col">Nama Pendonasi</th>
                            <th scope="col">Nominal Donasi</th>
                            <th scope="col">Metode Pembayaran</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($donations as $donation)
                        <tr>
                            <td style="color: blue;">#{{ $donation->invoice->no_invoice }}</td>
                            <td>{{ $donation->program->title }}</td>
                            <td>{{ $donation->invoice->user->name }}</td>
                            <td>{{ $donation->invoice->pay_amount }}</td>
                            <td>{{ $donation->payment_type }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection
