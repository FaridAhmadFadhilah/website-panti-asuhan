@extends('layouts.admin')
@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 30px;">

        <div class="card" style="width: 63vw;">
            <div class="row g-0">
                <div class="col-md-4">
                    <img src="https://www.alchinlong.com/wp-content/uploads/2015/09/sample-profile.png" class="img-fluid rounded-start" alt="...">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><b>{{ $anaks->nama_anak }}</b></h5>

                        <div class="row" style="margin-bottom: -15px;">
                            <div class="col-md-5">Tempat Lahir</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->tempat_lahir }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Tanggal Lahir</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->tgl_lahir }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Jenis KeLamin</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->jk }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Agama</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->agama }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Pendidikan</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->pendidikan_anak }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Status</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->status }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Nama Ayah</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->nama_ayah }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Nama Ibu</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->nama_ibu }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Pekerjaan Ayah</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->pekerjaan_ayah }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Pekerjaan Ibu</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->pekerjaan_ibu }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Asal Daerah Ayah</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->asal_daerah_ayah }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Asal Daerah Ibu</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->asal_daerah_ibu }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Alamat Orang Tua</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->alamat_ortu }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Tanggal Masuk</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->tgl_masuk }}</p>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: -15px">
                            <div class="col-md-5">Keterangan</div>
                            <div class="col-md-7">
                                <p class="">: {{ $anaks->keterangan }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
