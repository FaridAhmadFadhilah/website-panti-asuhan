@extends('layouts.admin')
@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">
            <div class="card-header text-center">
                <h5>Tambah Data Anak</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('anak.store') }}" method="POST" accept="">
                    @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Nama Lengkap
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('nama_anak') }}" class="form-control" name="nama_anak"
                                placeholder="Masukan Nama Lengkap">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Tempat Lahir
                        </div>
                        <div class="col-lg-9">
                            <input type="text" value="{{ old('tempat_lahir') }}" class="form-control"
                                name="tempat_lahir" placeholder="Masukan Tempat Lahir">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Tanggal Lahir
                        </div>
                        <div class="col-lg-9">
                            <input type="date" value="{{ old('tgl_lahir') }}" class="form-control" name="tgl_lahir">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Jenis Kelamin
                        </div>
                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jk" value="Laki-laki">
                                <label class="form-check-label" for="gridRadios1">
                                    Laki-laki
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jk" value="Perempuan">
                                <label class="form-check-label" for="gridRadios1">
                                    Perempuan
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Agama
                        </div>
                        <div class="col-lg-9">
                            <select class="form-select" name="agama" aria-label="Default select example">
                                <option selected>Pilih</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Khonghucu">Konghucu</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Pendidikan
                        </div>
                        <div class="col-lg-9">
                            <select class="form-select" name="pendidikan_anak">
                                <option selected>Pendidikan</option>
                                <option value="Islam">SD</option>
                                <option value="Kristen">SMP</option>
                                <option value="Hindu">SMK</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Status
                        </div>

                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="status[]" type="checkbox" value="Yatim">
                                <label class="form-check-label">Yatim</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="status[]" type="checkbox" value="Piatu">
                                <label class="form-check-label">Piatu</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Nama Ayah
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('nama_ayah') }}" class="form-control" name="nama_ayah"
                                placeholder="Masukan Nama Ayah">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Nama Ibu
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('nama_ibu') }}" class="form-control" name="nama_ibu"
                                placeholder="Masukan Nama Ibu">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Pekerjaan Ayah
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('pekerjaan_ayah') }}" class="form-control"
                                name="pekerjaan_ayah" placeholder="Masukan Pekerjaan Ayah">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Pekerjaan Ibu
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('pekerjaan_ibu') }}" class="form-control"
                                name="pekerjaan_ibu" placeholder="Masukan Pekerjaan Ibu">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Asal Daerah Ayah
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('asal_daerah_ayah') }}" class="form-control"
                                name="asal_daerah_ayah" placeholder="Masukan Asal Daerah Ayah">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Asal Daerah Ibu
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ old('asal_daerah_ibu') }}" class="form-control"
                                name="asal_daerah_ibu" placeholder="Masukan Asal Daerah Ibu">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Alamat Orang Tua
                        </div>

                        <div class="col-lg-9">
                            <input type="textarea" value="{{ old('alamat_ortu') }}" class="form-control"
                                name="alamat_ortu" placeholder="Masukan Alamat Orang Tua">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Tanggal Masuk
                        </div>
                        <div class="col-lg-9">
                            <input type="date" value="{{ old('tgl_masuk') }}" class="form-control" name="tgl_masuk">
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Keterangan
                        </div>

                        <div class="col-lg-9">
                            <input type="textarea" value="{{ old('keterangan') }}" class="form-control"
                                name="keterangan">
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-danger">Simpan Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection
