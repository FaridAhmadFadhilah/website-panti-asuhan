@extends('layouts.admin')
@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw">
            <div class="card-header text-center">
                <h5>Edit data</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('anak.update', $anaks->id) }}" method="post" accept="">
                    @csrf
                    @method('PUT')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Nama Lengkap
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->nama_anak }}" class="form-control" name="nama_anak"
                                placeholder="Masukan Nama Lengkap" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Tempat Lahir
                        </div>
                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->tempat_lahir }}" class="form-control"
                                name="tempat_lahir" placeholder="Masukan Tempat Lahir" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Tanggal Lahir
                        </div>
                        <div class="col-lg-9">
                            <input type="date" value="{{ $anaks->tgl_lahir }}" class="form-control" name="tgl_lahir"
                                required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Jenis Kelamin
                        </div>
                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jk" value="Laki-laki"
                                    {{ $anaks->jk == 'Laki-laki' ? 'checked' : '' }}>
                                <label class="form-check-label">
                                    Laki-laki
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="jk" value="Perempuan"
                                    {{ $anaks->jk == 'Perempuan' ? 'checked' : '' }}>
                                <label class="form-check-label">
                                    Perempuan
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Agama
                        </div>
                        <div class="col-lg-9">
                            <select class="form-select" name="agama" aria-label="Default select example">
                                <option selected>Pilih</option>
                                <option value="Islam" {{ $anaks->agama == 'Islam' ? 'selected' : '' }}>Islam</option>
                                <option value="Kristen" {{ $anaks->agama == 'Kristen' ? 'selected' : '' }}>Kristen
                                </option>
                                <option value="Hindu" {{ $anaks->agama == 'Hindu' ? 'selected' : '' }}>Hindu</option>
                                <option value="Budha" {{ $anaks->agama == 'Budha' ? 'selected' : '' }}>Budha</option>
                                <option value="Khonghucu" {{ $anaks->agama == 'Khonghucu' ? 'selected' : '' }}>Konghucu
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Pendidikan
                        </div>
                        <div class="col-lg-9">
                            <select class="form-select" name="pendidikan_anak">
                                <option selected>Pendidikan</option>
                                <option value="SD" {{ $anaks->pendidikan_anak == 'SD' ? 'selected' : '' }}>SD</option>
                                <option value="SMP" {{ $anaks->pendidikan_anak == 'SMP' ? 'selected' : '' }}>SMP</option>
                                <option value="SMK" {{ $anaks->pendidikan_anak == 'SMK' ? 'selected' : '' }}>SMK</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Status
                        </div>

                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="status[]" type="checkbox" value="Yatim"
                                    {{ $anaks->status == 'Yatim' ? 'checked' : '' }}>
                                <label class="form-check-label">Yatim</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="status[]" type="checkbox" value="Piatu"
                                    {{ $anaks->status == 'Piatu' ? 'checked' : '' }}>
                                <label class="form-check-label">Piatu</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Nama Ayah
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->nama_ayah }}" class="form-control" name="nama_ayah"
                                placeholder="Masukan Nama Ayah" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Nama Ibu
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->nama_ibu }}" class="form-control" name="nama_ibu"
                                placeholder="Masukan Nama Ibu" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Pekerjaan Ayah
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->pekerjaan_ayah }}" class="form-control"
                                name="pekerjaan_ayah" placeholder="Masukan Pekerjaan Ayah" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Pekerjaan Ibu
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->pekerjaan_ibu }}" class="form-control"
                                name="pekerjaan_ibu" placeholder="Masukan Pekerjaan Ibu" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Asal Daerah Ayah
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->asal_daerah_ayah }}" class="form-control"
                                name="asal_daerah_ayah" placeholder="Masukan Asal Daerah Ayah" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Asal Daerah Ibu
                        </div>

                        <div class="col-lg-9">
                            <input type="text" value="{{ $anaks->asal_daerah_ibu }}" class="form-control"
                                name="asal_daerah_ibu" placeholder="Masukan Asal Daerah Ibu" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Alamat Orang Tua
                        </div>

                        <div class="col-lg-9">
                            <input type="textarea" value="{{ $anaks->alamat_ortu }}" class="form-control"
                                name="alamat_ortu" placeholder="Masukan Alamat Orang Tua" required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Tanggal Masuk
                        </div>
                        <div class="col-lg-9">
                            <input type="date" value="{{ $anaks->tgl_masuk }}" class="form-control" name="tgl_masuk"
                                required>
                        </div>
                    </div>

                    <div class="form-group d-flex">
                        <div class="col-lg-3">
                            Keterangan
                        </div>

                        <div class="col-lg-9">
                            <input type="textarea" value="{{ $anaks->keterangan }}" class="form-control"
                                name="keterangan" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-danger">Ubah Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
