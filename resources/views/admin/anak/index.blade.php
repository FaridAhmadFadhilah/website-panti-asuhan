@extends('layouts.admin')
@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px">

        <div class="card" style="width: 70vw;">
            <div class="card-body">
                <div class="card-title text-center">
                    <h5>Management Pendataan Anak</h5>
                </div>
                @can('anak-create')
                    <a href="{{ route('anak.create') }}" class="btn btn-primary btn-sm float-left mb-3">Tambah Data
                        Anak</a>
                @endcan
                <div class="form-group">
                    <table class="table table-inverse">
                        <thead class="thead-invers">
                            <tr>
                                <th>No</th>
                                <th>Nama Anak</th>
                                <th>Status</th>
                                <th>Jenis Kelamin</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($anaks as $index => $anak)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $anak->nama_anak }}</td>
                                    <td>{{ $anak->status }}</td>
                                    <td>{{ $anak->jk }}</td>
                                    <td class="d-flex">
                                        @can('anak-show')
                                            <a href="{{ route('anak.show', $anak->id) }}"
                                                class="btn btn-outline-primary">Show</a>
                                        @endcan
                                        @can('anak-edit')
                                            <a href="{{ route('anak.edit', $anak->id) }}"
                                                class="btn btn-outline-warning">Edit</a>
                                        @endcan
                                        @can('anak-delete')
                                            <form action="{{ route('anak.destroy', $anak->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-outline-danger" type="submit"
                                                    onclick="return(confirm(`Apakah anda yakin akan menghapus?`))">Delete</button>
                                            </form>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach


                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@endsection
