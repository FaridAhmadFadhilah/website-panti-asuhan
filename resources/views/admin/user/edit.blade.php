@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">
            <div class="card-body">
                <div class="card-title text-center">Edit Users : <i>{{ $user->name }}</i></div>

                <form action="{{ route('user.update', $user->username) }}" method="post">
                    @csrf
                    @method('PUT')

                    <div class="mb-4">
                        <label for="name" class="form label">Masukan Nama Lengkap</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
                        <span class="text-danger">
                            <i>
                                @error('name')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <div class="mb-4">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}">
                        <span class="text-danger">
                            <i>
                                @error('email')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>


                    <div class="mb-4">
                        <label for="roles" class="form-label">Pilih Role</label>
                        <select name="roles[]" id="roles" multiple='multiple' class="form-control">
                            @foreach ($userRole as $ur)
                                <option value="{{ $ur }}" selected>{{ $ur }}</option>
                            @endforeach
                            @foreach ($roles as $role)
                                <option value="{{ $role }}">{{ $role }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">
                            <i>
                                @error('roles')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <button type="submit" class="btn btn-primary">Edit User</button>

                </form>

            </div>
        </div>
        
    </div>

@endsection
