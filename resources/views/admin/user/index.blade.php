@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

            <div class="card" style="width: 70vw">
                <div class="card-body">
                    <div class="card-title text-center">Management Users</div>
                    @can('user-create')
                    <a href="{{ route('user.create') }}" class="btn btn-primary my-4">Tambah Users</a>
                    @endcan

                    <div class="table-responsive">
                        <table class="table table-striped">

                            <thead>
                                <tr>
                                    <th class="col">No</th>
                                    <th class="col">Nama</th>
                                    <th class="col">Email</th>
                                    <th class="col">Role</th>
                                    <th class="col">Aksi</th>
                                </tr>
                            </thead>

                            <tbody>

                                @foreach ($users as $index => $user)

                                    <tr>

                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>
                                            @if ($user->getRoleNames())
                                                @foreach ($user->getRoleNames() as $role)
                                                    <label for="" class="badge badge-success">{{ $role }}</label>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="d-flex">
                                            @can('user-edit')
                                            <a href="{{ route('user.edit', $user->username) }}" class="btn btn-primary">Edit</a>
                                            @endcan
                                            @can('user-delete')
                                            <form action="{{ route('user.destroy', $user->username) }}" method="post">
                                            @csrf
                                            @method('delete')

                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')">Delete</button>
                                            </form>
                                            @endcan

                                        </td>

                                    </tr>

                                @endforeach

                            </tbody>

                        </table>
                    </div>

                </div>
            </div>

    </div>

@endsection
