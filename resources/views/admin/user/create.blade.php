@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">
            <div class="card-body">
                <div class="card-title text-center">Tambah Users</div>

                <form action="{{ route('user.store') }}" method="post">
                    @csrf

                    <div class="mb-4">
                        <label for="name" class="form label">Masukan Nama Lengkap</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                        <span class="text-danger">
                            <i>
                                @error('name')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <div class="mb-4">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}">
                        <span class="text-danger">
                            <i>
                                @error('email')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <div class="mb-4">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" name="username" id="username" class="form-control"
                            value="{{ old('username') }}">
                        <span class="text-danger">
                            <i>
                                @error('username')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <div class="mb-4">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" id="password" class="form-control">
                        <span class="text-danger">
                            <i>
                                @error('password')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <div class="mb-4">
                        <label for="roles" class="form-label">Pilih Role</label>
                        <select name="roles[]" id="roles" class="form-control" multiple>
                            @foreach ($roles as $role)
                                <option value="{{ $role }}">{{ $role }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">
                            <i>
                                @error('roles')
                                    {{ $message }}
                                @enderror
                            </i>
                        </span>
                    </div>

                    <button type="submit" class="btn btn-primary">Tambah</button>

                </form>

            </div>
        </div>

    </div>

@endsection
