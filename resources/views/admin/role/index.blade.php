@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw">
            <div class="card-body">
                <div class="card-title text-center">Management Role</div>
                @can('role-create')
                    <a href="{{ route('role.create') }}" class="btn btn-primary my-2">Tambah Role</a>
                @endcan
                <div class="table-responsive col-md-11">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="">No.</th>
                                <th class="">Nama</th>
                                <th class="">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $index => $role)

                                <tr>

                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td class="d-flex">
                                        @can('role-show')
                                            <a href="{{ route('role.show', $role->id) }}"
                                                class="btn btn-info text-white">Show</a>
                                        @endcan
                                        @can('role-edit')
                                            <a href="{{ route('role.edit', $role->id) }}" class="btn btn-primary">Edit</a>
                                        @endcan
                                        @can('role-delete')
                                            <form action="{{ route('role.destroy', $role->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger"
                                                    onclick="return confirm('Apakah anda yakin akan menghapus data ini ?')">Delete</button>
                                            </form>
                                        @endcan
                                    </td>

                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

@endsection
