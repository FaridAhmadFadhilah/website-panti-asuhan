@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">
            <form action="{{ route('role.update', $role->id) }}" method="post">
                @csrf
                @method('put')
                <div class="card-body">
                    <div class="card-title text-center">Edit Role {{ $role->name }}</div>

                    <div class="mb-4">
                        <label for="name" class="form-label">Nama Role</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $role->name }}">
                    </div>


                    <div class="mb-4">
                        @foreach ($permissions as $permission)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="permissions[]"
                                    value="{{ $permission->name }}" @foreach ($rp as $name){{ $name == $permission->name ? 'checked' : '' }}@endforeach>
                                <label class="form-check-label" for="flexCheckDefault">
                                    {{ $permission->name }}
                                </label>

                            </div>

                        @endforeach

                    </div>

                    <button type="submit" class="btn btn-primary">Edit</button>

                </div>
            </form>

        </div>

    </div>

@endsection
