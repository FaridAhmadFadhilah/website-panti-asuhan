@extends('layouts.admin')

@section('main-content')

    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw">
            <form action="{{ route('role.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="card-title">Menambahkan Role</div>

                    <div class="mb-4">
                        <label for="name" class="form-label">Nama Role</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>


                    <div class="mb-4">
                        @foreach ($permissions as $permission)
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="permissions[]"
                                    value="{{ $permission->name }}" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    {{ $permission->name }}
                                </label>
                            </div>
                        @endforeach
                    </div>

                    <button type="submit" class="btn btn-primary">Tambah Role</button>

                </div>
            </form>

        </div>

    </div>

@endsection
