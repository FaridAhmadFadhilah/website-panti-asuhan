@extends('layouts.admin')

@section('main-content')
    <div class="d-flex justify-content-center" style="padding-top: 50px;">

        <div class="card" style="width: 70vw;">
            <div class="card-body">
                <div class="card-title text-center">Detail Role <i>{{ $role->name }}</i></div>

                <div class="mb-4 d-flex">
                    <div class="col-md-4">Nama Role</div>
                    :
                    <div class="col-md-8">{{ $role->name }}</div>
                </div>

                <div class="mb-4 d-flex">
                    <label for="permission" class="col-md-4">Permission</label>

                    <ul class="col-md-8" id="permission">
                        @foreach ($rolePermissions as $permission)

                            <li>{{ $permission->name }}</li>

                        @endforeach
                    </ul>
                </div>

            </div>
        </div>

    </div>
@endsection
