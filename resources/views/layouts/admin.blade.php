<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{ asset('backend/css/argon.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/app.css') }}">
</head>
<body>
    <x-admin.sidebar/>
    <div>
        <x-admin.top-nav />
        <div class="d-flex" style="padding-left: 28%;">
            @yield('main-content')
        </div>
    </div>

    @if (Session::has('success'))
    <div class="fixed inset-0 flex justify-start items-end h-screen ">
        <div class="w-full">
            <x-alert type="success" :message="Session::get('success')"/>
        </div>
    </div>
    @endif

    <!--Core-->
    <script src="{{ asset('backend/js/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('backend/js/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('backend/js/vendor/js-cookie/js.cookie.js') }}"></script>
    <script src="{{ asset('backend/js/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('backend/js/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('backend/js/argon.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
