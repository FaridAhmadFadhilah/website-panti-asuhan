@extends('layouts.app')

@section('content')

    <div class="container pt-24">
        <a href="{{ route('user-program.index') }}" class="text-2xl  font-bold">Program Kebaikan</a>

        <div class="flex flex-wrap">

            @foreach ($programs as $program)
                @if ($program->due_date >= now())
                    <div class="lg:w-4/12 md:w-5/12 w-11/12" x-data="{value:0, total:{{ $program->donation }}}">

                        @include('components.card-program')

                    </div>
                @endif

            @endforeach

        </div>

        <a href="{{ route('user-program.index') }}" class="text-2xl  font-bold">Program yang sudah dilaksanakan</a>
        <div class="flex flex-wrap">
            @foreach ($programs as $program)
                @if ($program->due_date <= now())

                    <div class="lg:w-4/12 md:w-5/12 w-11/12">

                        @include('components.card-program')

                    </div>
                @endif

            @endforeach
        </div>
    </div>

@endsection
