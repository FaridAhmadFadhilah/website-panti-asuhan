@extends('layouts.app')

@section('content')

    <div x-data="{popUp:false}">

        <div class="flex flex-wrap pt-11 container">

            <div class="lg:w-8/12 w-11/12">
                <div class="bg-white shadow-lg container rounded-lg leading-relaxed  p-6 ">
                    <a href="{{ route('user-program.show', $program->slug) }}"
                        class="text-2xl font-bold text-center">{{ $program->title }}</a>
                    <p class="text-gray-400 text-sm my-4">{{ $program->updated_at->translatedFormat('d F Y, H:i') }}</p>
                    @if ($program->thumbnail)
                        <img src="{{ asset('storage/' . $program->thumbnail) }}" alt="" srcset="">
                    @else
                        <img src="{{ asset('frontend/img/DSHSH.jpg') }}" alt="" srcset="">
                    @endif
                    <div class="flex p-4 justify-between">
                        <div class="">
                            <span class="block">Uang Terkumpul</span>
                            <span class="text-lg font-semibold italic">Rp.
                                {{ number_format($donations->sum('invoice.pay_amount'), 2, ',', ',') }}
                            </span>
                        </div>
                        <div>
                            <span class="block">Sisa Waktu</span>
                            <span class="font-semibold">
                                @if ($program->due_date >= now())
                                    {{ \Carbon\Carbon::parse($program->due_date)->diffForHumans() }}
                                @else
                                    Sudah mencapai batas waktu
                                @endif
                            </span>
                        </div>

                    </div>
                    <button x-on:click="popUp = !popUp"
                        class="bg-gradient-to-tl from-red-600 py-3  to-yellow-700 text-white flex justify-center rounded-b-xl w-full">Donasi
                        Sekarang!</button>
                    <h1 class="font-bold text-3xl my-5">Cerita</h1>
                    <p class="text-justify font-sans text-gray-700 leading-relaxed">
                        {!! $program->body !!}
                    </p>
                </div>
            </div>

            <div class="lg:w-4/12 w-11/12">
                <div class="bg-white shadow-lg rounded-lg p-6 lg:ml-4">

                    <h2 class="font-bold text-2xl">Donasi</h2>
                    @foreach ($donations as $donation)
                        <div class="bg-white shadow-sm rounded flex px-7 py-4 my-4">

                            <div class="leading-relaxed">
                                <h3 class="font-medium text-sky-500 capitalize">{{ $donation->invoice->hidden_name ? 'Hamba Allah' : $donation->invoice->user->name }}</h3>
                                <p class="text-gray-800 text-sm">Berdonasi Sebesar Rp.
                                    <b class=" text-sky-500 italic">{{ number_format($donation->invoice->pay_amount, 2, ',', ',') }}</b>
                                </p>
                                <span class="text-gray-600 text-xs">{{ \Carbon\Carbon::parse($donation->paid_date)->translatedFormat('d F Y, H:i') }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
        @include('components.form-donation')

        <div class="bg-white shadow-md rounded-lg lg:w-11/12 w-9/12 container mt-12">
            <h2 class="text-2xl font-bold container py-2">Program Lainnya</h2>

            <div class="flex flex-wrap ">
                @foreach ($programs as $program)
                    <div class="lg:w-4/12">

                        @include('components.card-program')

                    </div>
                @endforeach

            </div>
        </div>

    </div>


@endsection
