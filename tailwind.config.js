const colors = require('tailwindcss/colors')
module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {

        container: {
            center: true,
            padding: '2rem',
        },

        colors:{
            rose: colors.rose,
            sky: colors.sky,
            emerald: colors.emerald,
            lime: colors.lime,
        },

    },
  },
  variants: {
    extend: {
        tableLayout: ['hover', 'focus'],
    },
  },
  plugins: [
      require('@tailwindcss/forms')
  ],
}
