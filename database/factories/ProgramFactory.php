<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProgramFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(5);
        return [
            'title' => $title,
            'slug' => Str::slug($title.' '.Str::random(10)),
            'body' => $this->faker->paragraph(),
            'donation' => $this->faker->randomDigitNotNull()*10000,
            'due_date' => $this->faker->dateTime()
        ];
    }
}
