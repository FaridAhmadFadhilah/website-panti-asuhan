<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{

    public function run()
    {
        Role::create([
            'name' => 'Super Admin',
            'guard_name' => 'web'
        ])->syncPermissions([
        'role-list', 'role-create', 'role-show', 'role-edit', 'role-delete',
        'user-list', 'user-create', 'user-edit', 'user-delete']);

        Role::create([
            'name' => 'Dewan Pembina',
            'guard_name' => 'web'
        ])->syncPermissions([
            'pengurus-list', 'anak-list', 'program-list', 'program-create', 'program-edit', 'program-delete'
        ]);

        Role::create([
            'name' => 'Ketua',
            'guard_name' => 'web'
        ])->syncPermissions(
            [
                'pengurus-list', 'pengurus-create', 'pengurus-show', 'pengurus-edit', 'pengurus-delete',
                'anak-list', 'anak-create', 'anak-show', 'anak-edit', 'anak-delete', 'role-list',
                'user-list', 'user-create', 'user-edit', 'user-delete',
                'program-list', 'program-create', 'program-edit', 'program-delete',
            ]
        );

        Role::create([
            'name' => 'Sekertaris',
            'guard_name' => 'web'
        ])->syncPermissions([
            'pengurus-list', 'pengurus-create', 'pengurus-edit', 'pengurus-delete',
            'anak-list', 'anak-create', 'anak-edit', 'anak-delete', 'role-list',
            'user-list', 'user-create', 'user-edit', 'user-delete',
            'program-list', 'program-create', 'program-edit', 'program-delete',
        ]);

        Role::create([
            'name' => 'Bendahara',
            'guard_name' => 'web'
        ])->syncPermissions([
            'keuangan-list', 'pengeluaran-create', 'pengeluaran-edit', 'pengeluaran-delete'
        ]);

        Role::create([
            'name' => 'Bagian Pengasuh',
            'guard_name' => 'web'
        ])->syncPermissions(['anak-list', 'anak-create', 'anak-edit', 'anak-show', 'anak-delete', 'pengurus-list', 'program-list']);

        Role::create([
            'name' => 'Member',
            'guard_name' => 'web'
        ]);
    }
}
