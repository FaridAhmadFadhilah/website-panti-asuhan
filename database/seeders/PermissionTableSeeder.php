<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permision = [
            ['name' => 'role-list', 'guard_name' => 'web'],
            ['name' => 'role-create', 'guard_name' => 'web'],
            ['name' => 'role-show', 'guard_name' => 'web'],
            ['name' => 'role-edit', 'guard_name' => 'web'],
            ['name' => 'role-delete', 'guard_name' => 'web'],

            ['name' => 'anak-list', 'guard_name' => 'web'],
            ['name' => 'anak-create', 'guard_name' => 'web'],
            ['name' => 'anak-show', 'guard_name' => 'web'],
            ['name' => 'anak-edit', 'guard_name' => 'web'],
            ['name' => 'anak-delete', 'guard_name' => 'web'],

            ['name' => 'pengurus-list', 'guard_name' => 'web'],
            ['name' => 'pengurus-create', 'guard_name' => 'web'],
            ['name' => 'pengurus-show', 'guard_name' => 'web'],
            ['name' => 'pengurus-edit', 'guard_name' => 'web'],
            ['name' => 'pengurus-delete', 'guard_name' => 'web'],


            ['name' => 'user-list', 'guard_name' => 'web'],
            ['name' => 'user-create', 'guard_name' => 'web'],
            ['name' => 'user-edit', 'guard_name' => 'web'],
            ['name' => 'user-delete', 'guard_name' => 'web'],

            ['name' => 'program-list', 'guard_name' => 'web'],
            ['name' => 'program-create', 'guard_name' => 'web'],
            ['name' => 'program-show', 'guard_name' => 'web'],
            ['name' => 'program-edit', 'guard_name' => 'web'],
            ['name' => 'program-delete', 'guard_name' => 'web'],

            ['name' => 'keuangan-list', 'guard_name' => 'web'],
            ['name' => 'pengeluaran-create', 'guard_name' => 'web'],
            ['name' => 'pengeluaran-edit', 'guard_name' => 'web'],
            ['name' => 'pengeluaran-delete', 'guard_name' => 'web'],
            ['name' => 'pengeluaran-show', 'guard_name' => 'web'],

        ];

        DB::table('permissions')->insert($permision);
    }
}
