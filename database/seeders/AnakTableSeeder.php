<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnakTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $anak= [
            ['nama_anak'=>'Alfi', 'tempat_lahir'=>'Bandung', 'tgl_lahir'=>'2003-10-11', 'jk'=>'Laki-laki',
            'agama'=>'Islam', 'pendidikan_anak'=>'SMK', 'status'=>'Yatim Piatu', 'nama_ayah'=>'Bagus', 'nama_ibu'=>'Siti',
            'pekerjaan_ayah'=>'Buruh', 'pekerjaan_ibu'=>'Ibu Rumah Tangga', 'asal_daerah_ayah'=>'Bogor',
            'asal_daerah_ibu'=>'Jakarta', 'alamat_ortu'=>'Jl. Cibaduyut', 'tgl_masuk'=>'2021-06-02',
            'keterangan'=>'Tidak Ada'],

            ['nama_anak'=>'Risma', 'tempat_lahir'=>'Sukabumi', 'tgl_lahir'=>'2003-12-09', 'jk'=>'Perempuan',
            'agama'=>'Islam', 'pendidikan_anak'=>'SMK', 'status'=>'Yatim', 'nama_ayah'=>'Cecep', 'nama_ibu'=>'Yulia',
            'pekerjaan_ayah'=>'Wirausaha', 'pekerjaan_ibu'=>'Ibu Rumah Tangga', 'asal_daerah_ayah'=>'Bandung',
            'asal_daerah_ibu'=>'Bandung', 'alamat_ortu'=>'Jl. Soekarno Hatta', 'tgl_masuk'=>'2021-06-02',
            'keterangan'=>'Tidak Ada'],

            ['nama_anak'=>'Rafli', 'tempat_lahir'=>'Garut', 'tgl_lahir'=>'2004-09-08',
            'jk'=>'Laki-laki', 'agama'=>'Islam', 'pendidikan_anak'=>'SMP', 'status'=>'Yatim',
            'nama_ayah'=>'Rudi', 'nama_ibu'=>'Nina', 'pekerjaan_ayah'=>'Karyawan', 'pekerjaan_ibu'=>'Ibu Rumah Tangga',
            'asal_daerah_ayah'=>'Bandung','asal_daerah_ibu'=>'Garut', 'alamat_ortu'=>'Garut', 'tgl_masuk'=>'2020-09-01',
            'keterangan'=>'Tidak Ada'],

        ];

        // masukan data ke database
        DB::table('anaks')->insert($anak);
    }


}
