<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PengurusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penguruses = [
            [
                'nama' => 'Yoyo', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Laki-laki', 'alamat' => 'Kp', 'jabatan' => 'Dewan Pembina', 'telepon' => '085', 'email' => 'yoyo@gmail.com',  'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Rista Hevilinda', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Perempuan', 'alamat' => 'Kp', 'jabatan' => 'Ketua', 'telepon' => '085', 'email' => 'rista@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Sandi Haryadi', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Laki-laki', 'alamat' => 'Kp.', 'jabatan' => 'Sekretaris', 'telepon' => '085', 'email' => 'sandi@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Eti Hestiningsih', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Perempuan', 'alamat' => 'Kp.', 'jabatan' => 'Bendahara', 'telepon' => '085', 'email' => 'eti@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Yana', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Laki-laki', 'alamat' => 'Kp.', 'jabatan' => 'Bidang Logistik', 'telepon' => '085', 'email' => 'yana@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Ustad Asep', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Laki-laki', 'alamat' => 'Kp.', 'jabatan' => 'Bidang Pendidikan', 'telepon' => '085', 'email' => 'asep@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Dr Ajat', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Laki-laki', 'alamat' => 'Kp.', 'jabatan' => 'Bidang Kesehatan', 'telepon' => '085', 'email' => 'ajat@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Nurhayati', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Perempuan', 'alamat' => 'Kp.', 'jabatan' => 'Bidang Perawatan', 'telepon' => '085', 'email' => 'nurhayati@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Lina', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Perempuan', 'alamat' => 'Kp.', 'jabatan' => 'Bagian Asrama', 'telepon' => '085', 'email' => 'lina@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Leni', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Perempuan', 'alamat' => 'Kp.', 'jabatan' => 'Bagian Dapur', 'telepon' => '085', 'email' => 'leni@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Yanti', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Perempuan', 'alamat' => 'Kp', 'jabatan' => 'Bagian Pengasuh', 'telepon' => '085', 'email' => 'yanti@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Dodi Hidayat AKS,M.Si', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => '', 'alamat' => 'Kp', 'jabatan' => 'Dewan Pembina', 'telepon' => '085', 'email' => 'dodi@gmail.com', 'agama' => 'Islam', 'status' => ''
            ],
            [
                'nama' => 'Drs Januar H Wewengkang', 'tempat_lahir' => 'Bandung', 'tanggal_lahir' => now(), 'jk' => 'Laki-laki', 'alamat' => 'Kp', 'jabatan' => 'Dewan Pembina', 'telepon' => '085', 'email' => 'januar@gmail.com', 'agama' => 'Islam', 'status' => ''
            ]
        ];

        DB::table('penguruses')->insert($penguruses);
    }
}
