<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $superAdmin =  User::create([
            'name' => 'Farid Ahmad Fadhilah',
            'username' => 'faridahmad',
            'email' => 'farid@admin.com',
            'password' => bcrypt('admin')
        ]);

        $superAdmin->assignRole('Super Admin');

        User::create([
            'name' => 'Yoyo Saryo',
            'username' => 'yoyosaryo',
            'email' => 'yoyo@gmail.com',
            'password' => bcrypt('admin')
        ])->assignRole('Dewan Pembina');

        User::create([
            'name' => 'Dodi Hidayat AKS,M.Si',
            'username' => 'dodihidayat',
            'email' => 'dodi@gmail.com',
            'password' => bcrypt('admin')
        ])->assignRole('Dewan Pembina');

        User::create([
            'name' => 'Drs Januar H Wewengkang',
            'username' => 'januarhw',
            'email' => 'januar@gmail.com',
            'password' => bcrypt('admin')
        ])->assignRole('Dewan Pembina');

        User::create([
            'name' => 'Rista Hevilinda',
            'username' => 'ristahevilinda',
            'email' => 'rista@gmail.com',
            'password' => bcrypt('admin')
        ])->assignRole('Ketua');

        User::create([
            'name' => 'Sandi Haryadi',
            'username' => 'sandiharyadi',
            'email' => 'sandi@gmail.com',
            'password' => bcrypt('admin')
        ])->assignRole('Sekertaris');

        User::create([
            'name' => 'Eti Hestiningsih',
            'username' => 'etihestiningsih',
            'email' => 'eti@gmail.com',
            'password' => bcrypt('admin')
        ])->assignRole('Bendahara');

        User::create([
            'name' => 'Yanti',
            'username' => 'yanti',
            'email' => 'yanti@gmail.com',
            'password' => bcrypt('Bagian Pengasuh')
        ])->assignRole('Bagian Pengasuh');

    }
}
