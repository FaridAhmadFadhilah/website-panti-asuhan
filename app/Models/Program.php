<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use HasFactory;

    protected $fillable = [
        'thumbnail', 'title', 'slug',
         'body', 'donation', 'due_date'
    ];

   public function getRouteKeyName()
   {
       return 'slug';
   }

}
