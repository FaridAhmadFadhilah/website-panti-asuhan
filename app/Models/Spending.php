<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spending extends Model
{
    use HasFactory;
    protected $fillable = ['description', 'amount', 'user_id', 'release_date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
