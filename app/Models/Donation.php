<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    use HasFactory;

    protected $fillable = ['invoice_id', 'program_id', 'payment_type', 'paid_date', 'status'];
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
    public function program()
    {
        return $this->belongsTo(Program::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }




}
