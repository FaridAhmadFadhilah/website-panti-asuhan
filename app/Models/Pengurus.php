<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengurus extends Model
{
    use HasFactory;
    protected $fillable = ['nama', 'email', 'tempat_lahir', 'tanggal_lahir', 'jk', 'alamat', 'jabatan', 'telepon', 'agama', 'status', 'profile'];

    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }

}
