<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    protected $fillable = ['no_invoice', 'program_id', 'hidden_name', 'user_id', 'status', 'pay_amount'];
    protected $casts = [
        'hidden_name' => 'boolean'
    ];

    public function getRouteKeyName()
    {
        return 'no_invoice';
    }
    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user associated with the Invoice
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function donation()
    {
        return $this->hasMany(Donation::class);
    }




}
