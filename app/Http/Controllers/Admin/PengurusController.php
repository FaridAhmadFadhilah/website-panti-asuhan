<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Pengurus\PengurusStoreRequest;
use App\Http\Requests\Pengurus\PengurusUpdateRequest;
use App\Models\Pengurus;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PengurusController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:pengurus-list|pengurus-create|pengurus-edit|pengurus-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:pengurus-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pengurus-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pengurus-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penguruses = Pengurus::get();
        return view('admin.pengurus.index', compact('penguruses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pengurus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengurusStoreRequest $request)
    {
    //    $user =  User::create([
    //         'name' => $request->nama,
    //         'email' => $request->email,
    //         'password' => bcrypt('admin')
    //     ]);

        if ($request->file('profile')) {
            $filename = Str::slug(now(), '') . '_Pengurus.' . $request->file('profile')->getClientOriginalExtension();
            $image = $request->file('profile')->storeAs('pengurus/profile', $filename);
        } else {
            $image = null;
        }

        Pengurus::create([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jk' => $request->jk,
            'alamat' => $request->alamat,
            'jabatan' => $request->jabatan,
            'telepon' => $request->telepon,
            'email' => $request->email,
            'agama' => $request->agama,
            'status' => $request->status,
            'profile' => $image
        ]);

        return redirect()->route('pengurus.index')->with('success', 'Pengurus berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Pengurus $penguru)
    {
        return view('admin.pengurus.show', compact('penguru'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengurus $penguru)
    {
        return view('admin.pengurus.edit', compact('penguru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengurusUpdateRequest $request, Pengurus $penguru)
    {
        if ($request->file('profile')) {
            if ($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $filename = Str::slug(now(), '') . '_Pengurus.' . $request->file('profile')->getClientOriginalExtension();
            $image = $request->file('profile')->storeAs('pengurus/profile', $filename);
        } else {
            $image = $request->oldImage;
        }

        $penguru->update([
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jk' => $request->jk,
            'alamat' => $request->alamat,
            'jabatan' => $request->jabatan,
            'email' => $request->email,
            'telepon' => $request->telepon,
            'agama' => $request->agama,
            'status' => $request->status,
            'profile' => $image,
        ]);

        return redirect()->route('pengurus.index')->with('success', 'Pengurus Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengurus $penguru)
    {
        if($penguru->profile){
            Storage::delete($penguru->profile);
        }

        $penguru->delete();

        return redirect()->route('pengurus.index');
    }
}
