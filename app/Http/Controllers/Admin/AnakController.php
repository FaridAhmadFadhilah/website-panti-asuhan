<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Anak;
use Illuminate\Http\Request;

class AnakController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:anak-list|anak-create|anak-edit|anak-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:anak-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:anak-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:anak-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anaks = Anak::orderBy('nama_anak')->get();
        return view('admin.anak.index', compact('anaks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { {
            return view('admin.anak.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_anak' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'pendidikan_anak' => 'required',
            'status' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'pekerjaan_ayah' => 'required',
            'pekerjaan_ibu' => 'required',
            'asal_daerah_ayah' => 'required',
            'asal_daerah_ibu' => 'required',
            'alamat_ortu' => 'required',
            'tgl_masuk' => 'required',
            'keterangan' => 'required',
        ]);

        Anak::create([
            'nama_anak' => $request->nama_anak,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jk' => $request->jk,
            'agama' => $request->agama,
            'pendidikan_anak' => $request->pendidikan_anak,
            'status' => implode($request->status),
            'nama_ayah' => $request->nama_ayah,
            'nama_ibu' => $request->nama_ibu,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'asal_daerah_ayah' => $request->asal_daerah_ayah,
            'asal_daerah_ibu' => $request->asal_daerah_ibu,
            'alamat_ortu' => $request->alamat_ortu,
            'tgl_masuk' => $request->tgl_masuk,
            'keterangan' => $request->keterangan
        ]);

        return redirect()->route('anak.index')->with('success', 'Data Anak berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anaks = Anak::findOrFail($id);
        return view('admin.anak.show', compact('anaks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anaks = Anak::findOrFail($id);
        return view('admin.anak.edit', compact('anaks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_anak' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jk' => 'required',
            'agama' => 'required',
            'pendidikan_anak' => 'required',
            'status' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'pekerjaan_ayah' => 'required',
            'pekerjaan_ibu' => 'required',
            'asal_daerah_ayah' => 'required',
            'asal_daerah_ibu' => 'required',
            'alamat_ortu' => 'required',
            'tgl_masuk' => 'required',
            'keterangan' => 'required',
        ]);

        $anaks = Anak::findOrFail($id);
        $anaks->nama_anak = $request->nama_anak;
        $anaks->tempat_lahir = $request->tempat_lahir;
        $anaks->tgl_lahir = $request->tgl_lahir;
        $anaks->jk = $request->jk;
        $anaks->agama = $request->agama;
        $anaks->pendidikan_anak = $request->pendidikan_anak;
        $anaks->status = implode($request->status);
        $anaks->nama_ayah = $request->nama_ayah;
        $anaks->nama_ibu = $request->nama_ibu;
        $anaks->pekerjaan_ayah = $request->pekerjaan_ayah;
        $anaks->pekerjaan_ibu = $request->pekerjaan_ibu;
        $anaks->asal_daerah_ayah = $request->asal_daerah_ayah;
        $anaks->asal_daerah_ibu = $request->asal_daerah_ibu;
        $anaks->alamat_ortu = $request->alamat_ortu;
        $anaks->tgl_masuk = $request->tgl_masuk;
        $anaks->keterangan = $request->keterangan;
        $anaks->save();

        return redirect()->route('anak.index')->with('success', 'Data Anak berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anak $anak)
    {
        $anak->delete();
        return redirect()->route('anak.index')->with('success', 'Data Anak Berhasil Dihapus');
    }
}
