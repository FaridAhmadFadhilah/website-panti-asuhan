<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Spending;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Pengeluaran\PengeluaranStoreRequest;
use App\Http\Requests\Pengeluaran\PengeluaranUpdateRequest;

class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spendings = Spending::get();
        return view('admin.keuangan.pengeluaran.index', compact('spendings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.keuangan.pengeluaran.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengeluaranStoreRequest $request)
    {
     Spending::create([
         'user_id' => Auth::user()->id,
         'description' => $request->description,
         'amount' => $request->amount,
         'release_date' => now()
     ]);

     return redirect()->route('pengeluaran.index')->with('success', 'Pengeluaran Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Spending $pengeluaran)
    {
        return view('admin.keuangan.pengeluaran.edit', compact('pengeluaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengeluaranUpdateRequest $request, Spending $pengeluaran)
    {
        $pengeluaran->update([
            'amount' => $request->amount,
            'description' => $request->description
        ]);

        return redirect()->route('pengeluaran.index')->with('success', 'Pengeluaran Telah Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Spending $pengeluaran)
    {
        $pengeluaran->delete();

        return redirect()->route('pengeluaran.index')->with('success', 'Pengeluaran Telah Dihapus');
    }
}
