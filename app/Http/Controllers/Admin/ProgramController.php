<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Program\ProgramStoreRequest;
use App\Http\Requests\Program\ProgramUpdateRequest;
use App\Models\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProgramController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:program-list|program-create|program-edit|program-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:program-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:program-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:program-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $programs = Program::orderBy('id', 'DESC')->get();
        return view('admin.program.index', compact('programs'));
    }


    public function create()
    {
        return view('admin.program.create');
    }


    public function store(ProgramStoreRequest $request)
    {

        if ($request->file('thumbnail')) {
            $filename = Str::slug(now(), '') . '_Program.' . $request->file('thumbnail')->getClientOriginalExtension();
            $image = $request->file('thumbnail')->storeAs('percubaan/image', $filename);
        } else {
            $image = null;
        }

        Program::create([
            'thumbnail' => $image,
            'title' => $request->title,
            'slug' => Str::slug($request->title . ' ' . Str::random(10)),
            'body' => $request->body,
            'donation' => $request->donation,
            'due_date' => $request->due_date
        ]);
        return redirect()->route('user-program.index')->with('success', 'Data berhasil ditambahkan');
    }


    public function show(Program $program)
    {
        return view('admin.program.show', compact('program'));
    }

    public function edit(Program $program)
    {
        return view('admin.program.edit', ['program' => $program]);
    }

    public function update(Request $request, Program $program)
    {

        if ($request->file('thumbnail')) {
            if ($request->oldImage) {
                Storage::delete($request->oldImage);
            }
            $filename = Str::slug(now(), '') . '_Program.' . $request->file('thumbnail')->getClientOriginalExtension();
            $image = $request->file('thumbnail')->storeAs('percubaan/image', $filename);
        } else {
            $image = $request->oldImage;
        }


        $program->update([
            'thumbnail' => $image,
            'title' => $request->title,
            'body' => $request->body,
            'donation' => $request->donation,
            'due_date' => $request->due_date
        ]);

        return redirect()->route('user-program.index')->with('success', 'Data Berhasil Diedit');
    }

    public function destroy(Program $program)
    {
        if ($program->thumbnail) {
            Storage::delete($program->thumbnail);
        }

        $program->delete();

        return redirect()->route('user-program.index')->with('success', 'Data berhasil dihapus');
    }
}
