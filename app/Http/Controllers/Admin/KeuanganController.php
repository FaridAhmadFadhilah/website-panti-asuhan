<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Donation;
use App\Models\Spending;
use Illuminate\Http\Request;

class KeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:keungan-list|pengeluaran-create|pengeluaran-edit|pengeluaran-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:pengeluaran-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:pengeluaran-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:pengeluaran-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $donations = Donation::get();
        $spendings = Spending::get();
        return view('admin.keuangan.index', compact('donations', 'spendings'));
    }

}
