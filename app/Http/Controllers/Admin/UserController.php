<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }


    public function index(User $user)
    {
        $users = User::get();
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::pluck('name');
        return view('admin.user.create', compact('roles'));
    }

    public function store(UserStoreRequest $request)
    {
       $user =  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password)
        ]);

        $user->assignRole($request->roles);

        return redirect()->route('user.index')->with('success', 'Users Berhasil Ditambahkan');
    }


    public function edit(User $user)
    {
        $roles = Role::pluck('name')->all();
        $userRole = $user->roles->pluck('name')->all();

        return view('admin.user.edit', compact('user', 'userRole','roles'));
    }

    public function update(UserUpdateRequest $request, User $user)
    {
       $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

     DB::table('model_has_roles')->where('model_id', $user->id)->delete();



        $user->assignRole($request->roles);

        return redirect()->route('user.index')->with('success', 'User Berhasil di update');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('user.index')->with('success', 'Data Berhasil dihapus');
    }
}
