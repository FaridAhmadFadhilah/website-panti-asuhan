<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Donation;
use Illuminate\Http\Request;

class PemasukanController extends Controller
{
    public function index()
    {
        $donations = Donation::get();
        return view('admin.keuangan.pemasukan.index', compact('donations'));
    }

}
