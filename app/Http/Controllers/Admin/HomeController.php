<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:Super Admin|Dewan Pembina|Ketua|Sekertaris|Bendahara|Bidang Logistik|Bidang Pendidikan|Bidang Kesehatan|Bidang Perawatan|Bagian Asrama|Bagian Dapur|Bagian Pengasuh');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }
}
