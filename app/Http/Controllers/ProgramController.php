<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\Program;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class ProgramController extends Controller
{
    public function index()
    {
        $programs = Program::orderBY('updated_at', 'DESC')->paginate(6);
        return view('program.index', compact('programs'));
    }

    public function show(Program $program)
    {

        $donations = Donation::with('invoice')->where('program_id', $program->id)->get();
        $programs = Program::orderBy('updated_at', 'DESC')->limit(3)->get();
        return view('program.show', compact('program', 'programs', 'donations'));
    }

    public function store(Request $request)
    {

        Invoice::create([
            'no_invoice' => random_int(100000000, 9999999999),
            'program_id' => $request->program_id,
            'user_id' => $request->user_id,
            'hidden_name' => $request->hidden_name,
            'status' => 'Belum Dibayar',
            'description' => $request->description,
            'pay_amount' => $request->pay_amount
        ]);

        return redirect()->route('index.invoice', Auth::user()->username)->with('success', 'Donasi berhasil');
    }
}
