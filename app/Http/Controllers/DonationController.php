<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DonationController extends Controller
{
    public function snap(Invoice $invoice)
    {
        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = env('MIDTRANS_SERVER_KEY');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;

        \Midtrans\Config::$overrideNotifUrl = env('APP_URL')."donation/hook";

        $params = collect([
            'transaction_details' => [
                'order_id' => $invoice->no_invoice,
                'gross_amount' => $invoice->pay_amount
            ],
            'customer_details' => [
                'first_name' => $invoice->user->name,
                'last_name' => '',
                'email' => $invoice->user->email,
            ],

        ]);

        try {
            $snapToken = \Midtrans\Snap::getSnapToken($params->toArray());
            return $snapToken;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function hook(Request $request)
    {
        if($request->transaction_status == "settlement"){

           $invoice =  Invoice::where('no_invoice', $request->order_id)->first();
           $invoice->status = 'Sudah Dibayar';
           $invoice->save();

           Donation::create([
            'invoice_id' => $invoice->id,
            'program_id' => $invoice->program_id,
            'paid_date' => $request->transaction_time,
            'status' => $invoice->status,
            'payment_type' => $request->payment_type,
           ]);

           Log::info(json_encode($invoice));
        }
    }

}
