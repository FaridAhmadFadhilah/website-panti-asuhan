<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function show(User $user)
    {
        $donations = Donation::get();
        // dd($donations);
        return view('user.profile', compact('user'));
    }

}
