<?php

namespace App\Http\Controllers;

use App\Models\Program;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __invoke()
    {
        $programs = Program::orderBy('updated_at', 'DESC')->limit(3)->get();
        return view('home', compact('programs'));
    }

}
