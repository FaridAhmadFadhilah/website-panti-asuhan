<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Invoice;

class InvoiceController extends Controller
{
    public function index(User $user)
    {
        $invoices = Invoice::where('user_id', $user->id)->get();
        return view('user.invoice.index', compact('user', 'invoices'));
    }

    public function show(User $user, Invoice $invoice)
    {
        \Midtrans\Config::$serverKey = env('MIDTRANS_SERVER_KEY');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;

        // try {
        //     $status_trans = \Midtrans\Transaction::status($invoice->no_invoice);
        // } catch (\Exception $e) {
        //     $status_trans['status_code'] = 404;
        //     $status_trans['status_message'] = "Transaction doesn't exist.";
        // }


        return view('user.invoice.show', compact('user', 'invoice'));
    }
}
