<?php

namespace App\Http\Requests\Pengurus;

use Illuminate\Foundation\Http\FormRequest;

class PengurusStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => ['required', 'string', 'max:255'],
            'tempat_lahir' => ['required', 'string'],
            'tanggal_lahir' => ['required'],
            'jk' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'jabatan' => ['required', 'string'],
            'telepon' => ['required'],
            'agama' => ['required', 'string'],
        ];
    }
}
