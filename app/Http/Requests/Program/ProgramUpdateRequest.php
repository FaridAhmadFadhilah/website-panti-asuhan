<?php

namespace App\Http\Requests\Program;

use Illuminate\Foundation\Http\FormRequest;

class ProgramUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thumbnail' => [ 'image', 'mimes:png,jpg,jpeg'],
            'title' => ['required'],
            'body' => ['required'],
            'donation' => ['required', 'integer'],
            'due_date' => ['required']
        ];
    }
}
