<?php

use App\Http\Controllers\Admin\AnakController;
use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\KeuanganController;
use App\Http\Controllers\Admin\PemasukanController;
use App\Http\Controllers\Admin\PengeluaranController;
use App\Http\Controllers\Admin\ProgramController as AdminProgramController;
use App\Http\Controllers\Admin\PengurusController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\UserController as ControllersUserController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::get('/', [AdminHomeController::class, 'index'])->name('admin.index');
    Route::prefix('keuangan')->group(function(){
        Route::get('/', [KeuanganController::class, 'index'])->name('keuangan.index');
        Route::get('pemasukan', [PemasukanController::class, 'index'])->name('pemasukan.index');
        Route::resource('pengeluaran', PengeluaranController::class)->except('show');
    });
    Route::resource('program', AdminProgramController::class)->except('show');
    Route::resource('pengurus', PengurusController::class);
    Route::resource('anak', AnakController::class);
    Route::resource('role', RoleController::class);
    Route::resource('user', UserController::class)->except('show');
});

Route::post('ckeditor/upload', [CKEditorController::class, 'upload'])->name('ckeditor.image-upload');
Route::get('{user}/profile', [ControllersUserController::class, 'show'])->name('profile');
Route::get('{user}/invoice', [InvoiceController::class, 'index'])->name('index.invoice');
Route::get('{user}/invoice/{invoice}', [InvoiceController::class, 'show'])->name('show.invoice');
Route::get('donation/pay/{invoice}', [DonationController::class, 'snap'])->name('donation.pay');
Route::post('donation/hook', [DonationController::class, 'hook'])->name('donation.hook');
Route::prefix('program')->group(function(){
    Route::get('/', [ProgramController::class, 'index'])->name('user-program.index');
    Route::get('{program}', [ProgramController::class, 'show'])->name('user-program.show');
    Route::middleware(['auth'])->group(function () {
    Route::post('{program}/donation', [ProgramController::class, 'store'])->name('donation.store');
    });
});

Auth::routes();

Route::get('/', HomeController::class)->name('home');

Route::get('/sto', fn () => storage_path());
